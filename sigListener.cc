//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <sigListener.h>

//Result Filter: used for dealing results with certain requirement
Register_ResultFilter("myFilter", sigResultFilter);
Register_Class(cMessage);

sigListener::sigListener() {
    // TODO Auto-generated constructor stub
    //outFile.open("MyRecord");
}

sigListener::sigListener(char *filePtr) {
    // TODO Auto-generated constructor stub
    if(outFile.is_open())
        outFile.close();
    outFile.open(filePtr);
}

sigListener::~sigListener() {
    // TODO Auto-generated destructor stub
}

void sigListener::receiveSignal(cComponent *src, simsignal_t id, cObject *obj)
{
    EV << "This is a test of the listener" << endl;
    MyUDPPacket *myPkt = dynamic_cast<MyUDPPacket*>(obj);
    EV << "Source: " << myPkt->getSourceAddr() << ", flowId: " << myPkt->getFlowId() << \
                   ", req: " << myPkt->getPktRequirement() << ", seq: " << myPkt->getSeqNo() << endl << endl;

    if (outFile.is_open())
    {
        outFile << "Source: " << myPkt->getSourceAddr() << ", flowId: " << myPkt->getFlowId() << \
                   ", req: " << myPkt->getPktRequirement() << ", seq: " << myPkt->getSeqNo() << endl << endl;
    }
}


void sigResultFilter::receiveSignal(cResultFilter *prev, simtime_t_cref t, cObject *object)
{
    EV << "This is a signal Result Filter test" << endl << endl << endl;
    MyUDPPacket *myPkt = dynamic_cast<MyUDPPacket*>(object);
    fire(this, t, (double)myPkt->getBitLength());

}

