//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "MyAppBase.h"

MyAppBase::MyAppBase() {
    // TODO Auto-generated constructor stub
    isOperational = false;
    myAddr = -1;
    destinationAddr = -2;
}

MyAppBase::~MyAppBase() {
    // TODO Auto-generated destructor stub
}

void MyAppBase::finish()
{
    stopApp();
    isOperational = false;
    //EV << "Successfully finish the task" << endl;
}

void MyAppBase::initialize(int stage)
{
    if (stage == 0) // STAGE_LOCAL
    {
        isOperational = true;
        if (isOperational);
            //std::cout << "MyAppBase 1st stage" << endl;
    }
    else if (stage == 1)    // NodeStatus gets parameters in stage 0.  // STAGE_LOCAL+1
    {
        isOperational = true;
        //std::cout << "MyAppBase 2nd stage" << endl;
    }
    else if (stage == 2) //TODO STAGE_APPLAYER
    {
        //std::cout << "MyAppBase 3rd stage" << endl;
        if (isOperational)
        {
            startApp();
            //std::cout << "Start" << endl;
        }
    }
}

void MyAppBase::handleMessage(cMessage *msg)
{
    if (isOperational)
        handleMessageWhenUp(msg);
    else
        handleMessageWhenDown(msg);
}

void MyAppBase::handleMessageWhenDown(cMessage *msg)
{
    if (msg->isSelfMessage())
        throw cRuntimeError("Model error: self msg '%s' received when isOperational is false", msg->getName());
    EV << "Application is turned off, dropping '" << msg->getName() << "' message\n";
    delete msg;
}
