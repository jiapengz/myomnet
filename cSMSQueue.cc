//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <cSMSQueue.h>

cSMSQueue::cSMSQueue(const char *name, MyCompareFunc cmp) : cQueueBase(name,cmp)
{
    // TODO Auto-generated constructor stub
    //EV << "cSMSQueue Constructor is called" << endl;
    enqueue_n = 0;
    dequeue_n = 0;
    useSlack = 0;
    queueType = 0;
    pktBefore = 0;
    qLengthBefore = 0;
    srvStartTime = 0;
    amptr = NULL;
}

cSMSQueue::~cSMSQueue() {
    // TODO Auto-generated destructor stub
    clear();
}

cSMSQueue::cSMSQueue(const cSMSQueue& queue) : cQueueBase(queue)
{
    copy(queue);
}

void cSMSQueue::copy(const cSMSQueue& queue)
{
    EV << "cSMSQueue Copy is called" << endl;
    enqueue_n = 0;
    dequeue_n = 0;
    useSlack = 0;
    queueType = 0;
    pktBefore = 0;
    amptr = NULL;
}

cSMSQueue& cSMSQueue::operator=(const cSMSQueue& queue)
{
    if (this==&queue) return *this;
    cQueueBase::operator=(queue);
    copy(queue);
    return *this;
}

std::string cSMSQueue::info() const
{
    if (empty())
        return std::string("empty");
    std::stringstream out;
    out << "len=" << getLength() << ", " << getBitLength() << " bits (" << getByteLength() << " bytes)";
    return out.str();
}

void cSMSQueue::insert(cObject *obj)
{
    if (!obj)
        throw cRuntimeError(this, "cannot insert NULL pointer");

    if (obj->isOwnedObject() && getTakeOwnership())
        take(static_cast<cOwnedObject *>(obj));

    testFunc1(obj);
    pktBefore = n;
    qLengthBefore = bitlength;

    EV << "Queue condition before insertion: ";
    testFunc2();


    if (!backp)
    {
        // insert as the only item
        QElem *e = new QElem;
        e->obj = obj;
        e->next = e->prev = NULL;
        frontp = backp = e;
        n = 1;
    }
    else if (queueType == 0/*compare==NULL*/)
    {
        EV << "Use FIFO enqueue" << endl;
        insafter_qelem(backp, obj);
    }
    else
    {
        // priority queue: seek insertion place
        EV << "Use SMS priority queue, previous service starts at: " << srvStartTime << endl;
        EV << "The difference is: " << simTime().dbl() - srvStartTime.dbl() << endl;
        EV << "The queue length before the new packet is: " << qLengthBefore << endl;
        QElem *p = backp;
        while (p && findFirstPlace(obj, p->obj) < 0/*compare(obj, p->obj) < 0*/)
        {
            EV << "The queue length before the new packet is: " << qLengthBefore << endl;
            p = p->prev;
            pktBefore--;
        }
        if (p)
            insafter_qelem(p, obj);
        else
            insbefore_qelem(frontp, obj);
    }
    testFunc2();
    testFunc3();
    enqueue_n++;
    addLength(obj);
}

cObject *cSMSQueue::pop()
{
    if (!frontp)
        throw cRuntimeError(this,"pop(): queue empty");

    dequeue_n++;

    return remove_qelem(frontp);
}

int cSMSQueue::findFirstPlace(cObject *newPacket, cObject *oldPacket)
{
    MyUDPPacket *newpkt_t = dynamic_cast<MyUDPPacket*>(newPacket);
    MyUDPPacket *oldpkt_t = dynamic_cast<MyUDPPacket*>(oldPacket);

    if(!newpkt_t)
        return 1;
    if(!oldpkt_t)
        return 1;

    if (newpkt_t->getKind() != LINK_DELAY_UPDATE && oldpkt_t->getKind() != LINK_DELAY_UPDATE)
    {
        if((newpkt_t->getSourceAddr() == oldpkt_t->getSourceAddr()) && (newpkt_t->getFlowId() == oldpkt_t->getFlowId()))
        {
            EV << "Same flow, should be in order." << endl;
            return 1;
        }

        double dt_new, dt_old, slack_old;
        dt_new = newpkt_t->getPktRequirement() - (simTime().dbl() - newpkt_t->getSendTime()) - newpkt_t->getDownStreamDelay()\
                - newpkt_t->getUpStreamDelay() - newpkt_t->getRealTimeDownStreamDelay();

        dt_old = oldpkt_t->getPktRequirement() - (simTime().dbl() - oldpkt_t->getSendTime()) - oldpkt_t->getDownStreamDelay()\
                - oldpkt_t->getUpStreamDelay() - oldpkt_t->getRealTimeDownStreamDelay();

        //slack_old = oldpkt_t->getPktRequirement() - (simTime().dbl() - oldpkt_t->getSendTime()) - oldpkt_t->getDownStreamDelay()\
                - oldpkt_t->getUpStreamDelay() - pktBefore * oldpkt_t->getBitLength() / linkrate;
        //slack_old = dt_old - pktBefore * oldpkt_t->getBitLength() / linkrate;
        //slack_old = dt_old - (qLengthBefore - oldpkt_t->getBitLength()) / linkrate;
        slack_old = dt_old - (qLengthBefore - oldpkt_t->getBitLength()) / linkrate - (simTime().dbl() - srvStartTime.dbl());
        //EV << "pktBefore = " << pktBefore << ", need " << pktBefore * oldpkt_t->getBitLength() / linkrate << "s" << endl;

        // pktBefore is the number of packets in the queue, but since there is one
        // transmitting on the link, the "pktBefore" variable can be seen as the
        // number of packets ahead of the "currently compared old packet"

        if (dt_new - dt_old < 0)
        {
            EV << "New packet is more urgent, should forward" << ", old dt: " << dt_old << ", old slack: " << slack_old << endl;
            if (useSlack && slack_old <  (oldpkt_t->getBitLength() + newpkt_t->getBitLength()) / linkrate)
            {
                EV << "But the old packet does not have enough slack time: " << slack_old << ", stop forward" << endl;
                return 2;
            }
            qLengthBefore -= oldpkt_t->getBitLength();
            return -1;
        }
        else
        {
            EV << "Old packet " << "(" << oldpkt_t->getSourceAddr() << "," << oldpkt_t->getFlowId() << "," << \
                    oldpkt_t->getSeqNo() << ")" << " is more urgent, stop forwarding" << endl;
            return 3;
        }
    }
    else if (newpkt_t->getKind() == LINK_DELAY_UPDATE && oldpkt_t->getKind() != LINK_DELAY_UPDATE)
    {
        if(!pmuFirst)
        {
            EV << "We give link delay update higher priority." << endl;
            qLengthBefore -= oldpkt_t->getBitLength();
            return -2;
        }
        else
        {
            EV << "We give PMU traffic higher priority." << endl;
            return 6;
        }
    }
    else if (oldpkt_t->getKind() == LINK_DELAY_UPDATE && newpkt_t->getKind() == LINK_DELAY_UPDATE)
    {
        EV << "Link delay updates are served in FIFO." << endl;
        return 4;
    }
    else if(oldpkt_t->getKind() == LINK_DELAY_UPDATE && newpkt_t->getKind() != LINK_DELAY_UPDATE)
    {
        if(pmuFirst)
        {
            EV << "We give PMU data higher priority, move forward." << endl;
            qLengthBefore -= oldpkt_t->getBitLength();
            return -3;
        }
        else
        {
            EV << "We give link delay update higher priority, stop forwarding." << endl;
            return 7;
        }
    }

    return 5;
}


void cSMSQueue::testFunc1(cObject *obj)
{
    MyUDPPacket *mypkt = dynamic_cast<MyUDPPacket*>(obj);
    EV << "Enqueue: "<< enqueue_n << ", dequeue: " << dequeue_n << ", No. of packets: " << n << ", queue-length: " << bitlength << endl;
    if (mypkt)
    {
        EV << "Source: " << mypkt->getSourceAddr() << ", flowId: " << mypkt->getFlowId() << \
                ", req: " << mypkt->getPktRequirement() << ", seq: " << mypkt->getSeqNo() << endl;
        EV << "Assumed upstream-delay: " << mypkt->getUpStreamDelay() << ", downstream: " << mypkt->getDownStreamDelay() << \
                " In the testFunc1" << endl;
    }
}

void cSMSQueue::testFunc2()
{
    QElem *pt = backp;
    MyUDPPacket *pkt;
    while (pt)
    {
        pkt = dynamic_cast<MyUDPPacket*>(pt->obj);
        if (pkt)
        {
            EV << "(" << pkt->getSourceAddr() << "," << pkt->getFlowId() << "," << pkt->getSeqNo() << ")->";
        }
        pt = pt->prev;
    }
    EV << endl;
}

void cSMSQueue::testFunc3()
{
    QElem *pt = backp;
    MyUDPPacket *pkt;
    while (pt)
    {
        pkt = dynamic_cast<MyUDPPacket*>(pt->obj);
        if (pkt)
        {
            if (pkt->getKind() != LINK_DELAY_UPDATE)
            {
                double dt = pkt->getPktRequirement() - (simTime().dbl() - pkt->getSendTime()) - pkt->getDownStreamDelay()\
                            - pkt->getUpStreamDelay() - pkt->getRealTimeDownStreamDelay();
                EV << "(" << pkt->getSourceAddr() << "," << pkt->getFlowId() << "," << pkt->getSeqNo() << ")=> ";
                EV << "D0: " << pkt->getPktRequirement() << ", du: " << (simTime().dbl() - pkt->getSendTime()) << \
                        ", delta_du: " << pkt->getUpStreamDelay() << ", delta_dd: " << pkt->getDownStreamDelay() << \
                        ", real-time dd: " << pkt->getRealTimeDownStreamDelay() << ", dt: " << dt << ", generated at "\
                        << pkt->getSendTime() << endl;
            }
            else
            {
                EV << "(" << pkt->getSourceAddr() << "," << pkt->getFlowId() << "," << pkt->getSeqNo() << ")=> ";
                EV << "An update from node: " << pkt->getSourceAddr() << endl;
            }
        }
        pt = pt->prev;
    }

}
