//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <MyStaticRtRec.h>

Define_Module(MyStaticRtRec);

MyStaticRtRec::MyStaticRtRec() {
    // TODO Auto-generated constructor stub

}

MyStaticRtRec::~MyStaticRtRec() {
    // TODO Auto-generated destructor stub
}

void MyStaticRtRec::initialize()
{
    EV << "Size of map is " << rtRecord.size() << endl;
}

void MyStaticRtRec::sayHello()
{
    EV << "Function called from other routing modules" << endl;
    EV << "Size of map is " << rtRecord.size() << endl;
}

void MyStaticRtRec::staticRtMapScan()
{
    std::map<int,std::map<int,int> >::iterator tempIt2;
    std::map<int,int>::iterator tempIt;

    EV << endl;
    for(tempIt2 = rtRecord.begin(); tempIt2 != rtRecord.end(); tempIt2++)
    {
        EV << "Static record for " << "Node " << tempIt2->first << ": " << endl;
        for(tempIt = tempIt2->second.begin(); tempIt != tempIt2->second.end(); tempIt++)
        {
            EV << "    Destination: Node " << tempIt->first << ", next node: " << tempIt->second << " " << endl;
        }
    }
}
