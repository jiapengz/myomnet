//
// Generated file, do not edit! Created by opp_msgc 4.4 from Routing/MyDelayExPktDV.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#include <iostream>
#include <sstream>
#include "MyDelayExPktDV_m.h"

USING_NAMESPACE

// Template rule which fires if a struct or class doesn't have operator<<
template<typename T>
std::ostream& operator<<(std::ostream& out,const T&) {return out;}

// Another default rule (prevents compiler from choosing base class' doPacking())
template<typename T>
void doPacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doPacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}

template<typename T>
void doUnpacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doUnpacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}




Register_Class(MyDelayExPktDV);

MyDelayExPktDV::MyDelayExPktDV(const char *name, int kind) : ::MyUDPPacket(name,kind)
{
    this->expectedNeighborDV_var = 0;
}

MyDelayExPktDV::MyDelayExPktDV(const MyDelayExPktDV& other) : ::MyUDPPacket(other)
{
    copy(other);
}

MyDelayExPktDV::~MyDelayExPktDV()
{
}

MyDelayExPktDV& MyDelayExPktDV::operator=(const MyDelayExPktDV& other)
{
    if (this==&other) return *this;
    ::MyUDPPacket::operator=(other);
    copy(other);
    return *this;
}

void MyDelayExPktDV::copy(const MyDelayExPktDV& other)
{
    this->expectedNeighborDV_var = other.expectedNeighborDV_var;
    this->neighborDelayTableDV_var = other.neighborDelayTableDV_var;
}

void MyDelayExPktDV::parsimPack(cCommBuffer *b)
{
    ::MyUDPPacket::parsimPack(b);
    doPacking(b,this->expectedNeighborDV_var);
    doPacking(b,this->neighborDelayTableDV_var);
}

void MyDelayExPktDV::parsimUnpack(cCommBuffer *b)
{
    ::MyUDPPacket::parsimUnpack(b);
    doUnpacking(b,this->expectedNeighborDV_var);
    doUnpacking(b,this->neighborDelayTableDV_var);
}

int MyDelayExPktDV::getExpectedNeighborDV() const
{
    return expectedNeighborDV_var;
}

void MyDelayExPktDV::setExpectedNeighborDV(int expectedNeighborDV)
{
    this->expectedNeighborDV_var = expectedNeighborDV;
}

DelayMapDV& MyDelayExPktDV::getNeighborDelayTableDV()
{
    return neighborDelayTableDV_var;
}

void MyDelayExPktDV::setNeighborDelayTableDV(const DelayMapDV& neighborDelayTableDV)
{
    this->neighborDelayTableDV_var = neighborDelayTableDV;
}

class MyDelayExPktDVDescriptor : public cClassDescriptor
{
  public:
    MyDelayExPktDVDescriptor();
    virtual ~MyDelayExPktDVDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(MyDelayExPktDVDescriptor);

MyDelayExPktDVDescriptor::MyDelayExPktDVDescriptor() : cClassDescriptor("MyDelayExPktDV", "MyUDPPacket")
{
}

MyDelayExPktDVDescriptor::~MyDelayExPktDVDescriptor()
{
}

bool MyDelayExPktDVDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<MyDelayExPktDV *>(obj)!=NULL;
}

const char *MyDelayExPktDVDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int MyDelayExPktDVDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 2+basedesc->getFieldCount(object) : 2;
}

unsigned int MyDelayExPktDVDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISCOMPOUND,
    };
    return (field>=0 && field<2) ? fieldTypeFlags[field] : 0;
}

const char *MyDelayExPktDVDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "expectedNeighborDV",
        "neighborDelayTableDV",
    };
    return (field>=0 && field<2) ? fieldNames[field] : NULL;
}

int MyDelayExPktDVDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='e' && strcmp(fieldName, "expectedNeighborDV")==0) return base+0;
    if (fieldName[0]=='n' && strcmp(fieldName, "neighborDelayTableDV")==0) return base+1;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *MyDelayExPktDVDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "DelayMapDV",
    };
    return (field>=0 && field<2) ? fieldTypeStrings[field] : NULL;
}

const char *MyDelayExPktDVDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int MyDelayExPktDVDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    MyDelayExPktDV *pp = (MyDelayExPktDV *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string MyDelayExPktDVDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    MyDelayExPktDV *pp = (MyDelayExPktDV *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getExpectedNeighborDV());
        case 1: {std::stringstream out; out << pp->getNeighborDelayTableDV(); return out.str();}
        default: return "";
    }
}

bool MyDelayExPktDVDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    MyDelayExPktDV *pp = (MyDelayExPktDV *)object; (void)pp;
    switch (field) {
        case 0: pp->setExpectedNeighborDV(string2long(value)); return true;
        default: return false;
    }
}

const char *MyDelayExPktDVDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        "DelayMapDV",
    };
    return (field>=0 && field<2) ? fieldStructNames[field] : NULL;
}

void *MyDelayExPktDVDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    MyDelayExPktDV *pp = (MyDelayExPktDV *)object; (void)pp;
    switch (field) {
        case 1: return (void *)(&pp->getNeighborDelayTableDV()); break;
        default: return NULL;
    }
}


