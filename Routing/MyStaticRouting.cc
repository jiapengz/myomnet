//A simple routing mechanism

#include <map>
#include <omnetpp.h>
#include "MyUDPPacket_m.h"
#include "SMSQueue.h"
#include "SimType.h"
#include "MyStaticRtRec.h"

struct pathRouteInfo;

/**
 * Demonstrates static routing, utilizing the cTopology class.
 */
class MyStaticRouting : public cSimpleModule
{
  private:
    int myAddress;
    int flowId;
    long sequence; // used for the update
    bool sendDelayUpdate; //whether this node will send delay update
    int routerNumber;
    std::string nodeType;
    myNodeKind deviceType;
    bool routingTest; // if true, the delay of each link is route_id * 0.001
    int routingType; //1 means link state, 2 means distance vector
    bool usePiggyback; // whether use piggyback to send update
    bool idealMeasure; // if true, packet will have 0 size
    bool prediction; // if true, try to predict the pattern of traffic
    bool multipaths; // if true, the topology is mesh or others that contain multiple paths

    simtime_t nextDataTime;
    simtime_t nextMeasureTime;

    typedef std::map<int,int> RoutingTable; // destaddr -> gateindex
    typedef std::map<int,double> NBTable;
    RoutingTable rtable;
    std::map<int,double> NeighborTable; // map<neighbor ID, link delay>
    std::map<int,double> updateNBTable;
    std::map<int,int> Queue_NB_MAP; // map<gate ID, neighbor ID>
    std::map<int,int> NB_Queue_MAP; // map<neighbor ID, gate ID>
    std::map<int,std::map<int,double> > TopologyMap; // topology of the network
    std::map<int,long> seqRecord; // record the received sequence number of the update for each node
    std::map<int,bool> Queue_IsSource_MAP; // map<gate ID, Is_Source>
    std::map<int,bool> Queue_IsCC_MAP; // map<ate ID, Is_Center>
    std::map<int,double> predictTable; // map<interface ID, next data arrive time>

    //Used for path computation
    //std::map<int,double> dist;
    std::map<int,pathRouteInfo> dist;
    std::map<int,bool> sContainer;
    //std::map<int,int> preNodeTable; // test the routing table containing the previous hop for a destination
    //std::map<int,int> nextHopTable; // a table containing the next hop of a path

    //Used for periodic reading link delay
    simtime_t startTime;
    simtime_t reportPeriod;
    simtime_t stopTime;
    cMessage *periodRead; //periodically compute the mean delay

    //Used for periodic path compute
    simtime_t pStartTime;
    simtime_t pReportPeriod;
    simtime_t pStopTime;
    cMessage *pUpdateCompute;

    //Used for periodic link delay update
    /*simtime_t updateStartTime;
    simtime_t updateReportPeriod;
    simtime_t updateStopTime;
    cMessage *periodLinkUpdate;*/

    cModule *modulePt;
    QueueBase *Ptr;
    MyStaticRtRec *staticRtRecPt;

    //simsignal_t dropSignal;
    //simsignal_t outputIfSignal;

  public:
    MyStaticRouting();
    ~MyStaticRouting();

  protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
    virtual void staticRtRec();

    virtual void readDelay();
    virtual void neighborDiscovery();
    virtual MyUDPPacket* generateUpdatePkt();
    virtual void storeNeighbor(cMessage *msg);
    virtual void showTable();
    virtual void sendUpdate();

    // Functions for link state
    virtual void handleLinkDelayUpdate(cMessage *msg);
    virtual void scanTopology();
    virtual void updateFlood(cMessage *msg);
    virtual void computePathDelay();
    virtual void showPathDelay();
    virtual void recordDelay(cMessage *msg);
    virtual void lookUpDownStreamDelay(cMessage *msg);

    // Functions for distance vector
    virtual void handleDVUpdate(cMessage *msg);
    virtual void updateSelfDV(int nextID, double delta_delay);
    virtual void computePathDelayDV(cMessage *msg);

    // Functions for piggyback
    virtual bool isPiggybacked(cMessage *msg, int gateID, int updateType);
};

Define_Module(MyStaticRouting);

MyStaticRouting::MyStaticRouting()
{
    periodRead = NULL;
    sequence = 1;
    sendDelayUpdate = 0;
}

MyStaticRouting::~MyStaticRouting()
{
    cancelAndDelete(periodRead);
    cancelAndDelete(pUpdateCompute);
}


void MyStaticRouting::initialize()
{
    startTime = par("rtStartTime").doubleValue();
    reportPeriod = par("rtReportPeriod").doubleValue();
    stopTime = par("rtStopTime").doubleValue();

    pStartTime = par("pathStartTime").doubleValue();
    pReportPeriod = par("pathReportPeriod").doubleValue();
    pStopTime = par("pathStopTime").doubleValue();

    sendDelayUpdate = par("sendDelayUpdate").boolValue();
    routerNumber = getParentModule()->par("totalRouter");
    EV << "Total " << routerNumber << " routers" << endl;

    routingTest = par("routingTest").boolValue();
    routingType = par("routingType");
    if(routingType == 1)
    {
        EV << "Using link state" << endl;
    }
    else if(routingType == 2)
    {
        EV << "Using distance vector" << endl;
    }

    usePiggyback = par("usePiggyback").boolValue();
    idealMeasure = par("idealMeasure").boolValue();
    prediction = par("prediction").boolValue();
    multipaths = par("meshTopology").boolValue();
    staticRtRecPt = NULL;

    nodeType = getParentModule()->par("nodeType").stringValue();
    if(nodeType.compare("source") == 0)
    {
        deviceType = SOURCE;
        EV << "This is a source" << " " << nodeType << endl;
    }
    else if(nodeType.compare("router") == 0)
    {
        deviceType = ROUTER;
        EV << "This is a router" << " " << nodeType << endl;
    }
    else
    {
        deviceType = CENTER;
        EV << "This is a center" << " " << nodeType << endl;
    }

    periodRead = new cMessage("delayRdEvent");
    pUpdateCompute = new cMessage("delayUpdateCompute");
    myAddress = getParentModule()->par("address");
    flowId = par("routingFlowId");

    nextMeasureTime = nextDataTime = -1;

    /*EV << "No. of out Gates: " << gateSize("out") << endl;
    EV << "No. of queues: " << getParentModule()->getSubmodule("queue",0)->getVectorSize() << endl;
    EV << "Full name: " << getParentModule()->getSubmodule("queue",0)->getFullName() << endl;
    EV << "Get name: " << getParentModule()->getSubmodule("queue",0)->getName() << endl;*/

    if(startTime > SIMTIME_ZERO && reportPeriod >SIMTIME_ZERO)
    {
        scheduleAt((startTime + reportPeriod), periodRead);
        nextMeasureTime = startTime + reportPeriod;
    }


    if(pStartTime > SIMTIME_ZERO && pReportPeriod >SIMTIME_ZERO)
    {
        scheduleAt((pStartTime + pReportPeriod), pUpdateCompute);
    }
    //dropSignal = registerSignal("drop");
    //outputIfSignal = registerSignal("outputIf");

    if(multipaths)
    {
        staticRtRec();
    }
    //
    // Brute force approach -- every node does topology discovery on its own,
    // and finds routes to all other nodes independently, at the beginning
    // of the simulation. This could be improved: (1) central routing database,
    // (2) on-demand route calculation
    //
    cTopology *topo = new cTopology("topo");

    std::vector<std::string> nedTypes;
    EV << "Test: " << getParentModule()->getNedTypeName() << endl << endl;

    //initialize the predict table
    int i;
    for(i = 0; i < gateSize("out"); i++)
    {
        predictTable[i] = -1;
    }

    /*const char *typeNames[3];
    typeNames[0] = "mysimulator.Node.MyRouter";
    typeNames[1] = "mysimulator.Node.MyNode";
    typeNames[2] = NULL;*/


    //nedTypes.push_back(getParentModule()->getNedTypeName());
    //topo->extractByNedTypeName(nedTypes);
    nedTypes.push_back("mysimulator.Node.MyRouter");
    nedTypes.push_back("mysimulator.Node.MyNode");
    topo->extractByNedTypeName(nedTypes);

    EV << "cTopology found " << topo->getNumNodes() << " nodes\n";

    cTopology::Node *thisNode = topo->getNodeFor(getParentModule());

    // find and store next hops
    for (int i=0; i<topo->getNumNodes(); i++)
    {
        if (topo->getNode(i)==thisNode) continue; // skip ourselves
        topo->calculateUnweightedSingleShortestPathsTo(topo->getNode(i));

        if (thisNode->getNumPaths()==0) continue; // not connected

        cGate *parentModuleGate = thisNode->getPath(0)->getLocalGate();
        int gateIndex = parentModuleGate->getIndex();
        int address = topo->getNode(i)->getModule()->par("address");
        rtable[address] = gateIndex;
        EV << "  towards address " << address << " gateIndex is " << gateIndex << endl;
        int nextAddr = thisNode->getPath(0)->getRemoteNode()->getModule()->par("address");
        EV << "The next node addr is " << nextAddr << endl;

        if(multipaths)
        {
            staticRtRecPt->rtRecord[myAddress].insert(std::make_pair(address,nextAddr));
        }
    }
    delete topo;

    if(deviceType == ROUTER && myAddress == 20)
    {
        staticRtRecPt->staticRtMapScan();
    }
    neighborDiscovery();
}

void MyStaticRouting::handleMessage(cMessage *msg)
{
    if (msg == periodRead)
    {
        readDelay();
        if(simTime() + reportPeriod <= stopTime)
        {
            scheduleAt((simTime() + reportPeriod), periodRead);
        }

        double updateTransTime;
        updateTransTime = (28 + 12 * updateNBTable.size()) * 8 / 480000.0;

        if (sendDelayUpdate)
        {
            if(idealMeasure || (!prediction) || nextDataTime < SIMTIME_ZERO || nextDataTime.dbl() > nextMeasureTime.dbl() + updateTransTime)
            {
                if(prediction)
                {
                    EV << "nextDataTime is " << nextDataTime.dbl() << ", measure sent out until " << nextMeasureTime \
                        + updateTransTime << endl;
                }
                sendUpdate();
            }
            else
            {
                if(prediction)
                {
                    EV << "nextDataTime is " << nextDataTime.dbl() << ", measure sent out until " << nextMeasureTime \
                        + updateTransTime << endl;
                }
                EV << "Stop update for data traffic" << endl;
            }
        }

        nextMeasureTime = simTime() + reportPeriod;
        EV << "Next measurement comes  at" << nextMeasureTime.dbl() << endl;
    }
    else if(msg == pUpdateCompute)
    {
        if(routingType == 1)
        {
            computePathDelay();
        }
        if(simTime() + pReportPeriod <= pStopTime)
        {
            scheduleAt((simTime() + pReportPeriod), pUpdateCompute);
        }
    }
    else // Packets coming from outside the node
    {
        MyUDPPacket *pk = check_and_cast<MyUDPPacket *>(msg);
        int destAddr = pk->getDestinationAddr();

        if(pk->getSourceAddr() != myAddress)
        {
            pk->setHopCount(pk->getHopCount()+1);
            EV << "This is the " << pk->getHopCount() << " hop" << endl;
        }


        // A piggy-backed packet is received
        if(pk->getKind() == PIGGY_BACK)
        {
            EV << "The pb packet is (" << pk->getFlowId() << "," << \
                    pk->getSeqNo() << ")" << endl;

            if(routingType == 1)
            {
                EV << "Before LS processing, the size is: " << pk->getNeighborDelayInfo().size() \
                        << ", the packet size is: " << pk->getByteLength() << endl;
                handleLinkDelayUpdate(msg);
                int newSize = pk->getByteLength() - 12 * pk->getNeighborDelayInfo().size();
                pk->setByteLength(newSize);
                pk->getNeighborDelayInfo().clear();
                EV << "After LS processing, the size is: " << pk->getNeighborDelayInfo().size() \
                        << ", the packet size becomes: " << pk->getByteLength() << endl;
            }
            else if(routingType == 2)
            {
                EV << "Before DV processing, the size is: " << pk->getNeighborDelayTable().size() << endl;
                handleDVUpdate(msg);
                pk->getNeighborDelayTable().clear();
                EV << "Before DV processing, the size is: " << pk->getNeighborDelayTable().size() << endl;
            }

            pk->setKind(pk->getPbUpdate().originalKind);
            EV << "Now the packet kind for " << "(" << pk->getFlowId() << "," << \
                    pk->getSeqNo() << ") is " << pk->getKind() << endl;
        }


        // Packets whose destination is the node itself
        if (destAddr == myAddress)
        {
            EV << "local delivery of packet " << pk->getName() << endl;
            send(pk, "udpsink");
            //emit(outputIfSignal, -1); // -1: local
            return;
        }
        else if (destAddr == NBD_ADDR)
        {
            storeNeighbor(msg);
            showTable();
            delete msg;
            return;
        }
        else if (destAddr == LINK_STATE_UPDATE_ADDR)
        {
            handleLinkDelayUpdate(msg);
            delete msg;
            return;
        }
        else if (destAddr == DISTANCE_VECTOR_UPDATE_ADDR)
        {
            handleDVUpdate(msg);

            std::map<int,long>::iterator tempIt3;
            for(tempIt3 = seqRecord.begin(); tempIt3 != seqRecord.end(); tempIt3++)
            {
                EV << "Sequence pair: " << tempIt3->first << " <-> " << tempIt3->second << endl;
            }
            delete msg;
            return;
        }

        RoutingTable::iterator it = rtable.find(destAddr);
        if (it==rtable.end())
        {
            EV << "address " << destAddr << " unreachable, discarding packet " << pk->getName() << endl;
            //emit(dropSignal, (long)pk->getByteLength());
            delete pk;
            return;
        }

        int outGateIndex = (*it).second;

        //compute the next possible data arrival time
        if(pk->getHopCount() == 1 && pk->getKind() == TRAFFIC)
        {
            double timeInterval = 1.0/30;
            nextDataTime = simTime() + timeInterval;
            EV << "Next cycle time " << nextDataTime.dbl() << endl;
            predictTable[outGateIndex] = nextDataTime.dbl();
        }
        //EV << "Msg from gate: " << msg->getArrivalGate()->getIndex() << endl;
        //EV << "Gate full name: " << msg->getArrivalGate()->getFullName() << endl;


        EV << "forwarding packet " << pk->getName() << " on gate index " << outGateIndex << endl;
        //pk->setHopCount(pk->getHopCount()+1);
        //emit(outputIfSignal, outGateIndex);
        if(pk->getKind() == TRAFFIC && pk->getSourceAddr() != myAddress && deviceType == ROUTER)
        {
            recordDelay(msg);
            lookUpDownStreamDelay(msg);
        }

        send(pk, "out", outGateIndex);
    }
}

void MyStaticRouting::readDelay()
{
    int gateNo = gateSize("out");
    int i;
    int temp_id;
    double tempLinkDelay;

    for(i=0; i<gateNo; i++)
    {
        modulePt = getParentModule()->getSubmodule("queue",i);
        Ptr = dynamic_cast<QueueBase*>(modulePt);
        if(Ptr)
        {
            Ptr->delayCompute();
            EV << "Read data: " << Ptr->mean_delay << ", from queue no. " << Ptr->getIndex() << endl;

            //If link is idle, set a minimum delay
            if(routingTest == 0)
            {
                tempLinkDelay = Ptr->mean_delay;
                if(!tempLinkDelay)
                {
                    tempLinkDelay = 196 * 8 / Ptr->linkrate;
                }
            }
            else
            {
                tempLinkDelay = 0.001 * myAddress;
            }


            temp_id = Queue_NB_MAP.find(i)->second;
            NeighborTable.find(temp_id)->second = tempLinkDelay;
            EV << "Read neighbor table: " << NeighborTable.find(temp_id)->second << endl;

            if(updateNBTable.find(temp_id) != updateNBTable.end())
            {
                updateNBTable.find(temp_id)->second = tempLinkDelay;
            }

            // If we use Distance Vector, we directly update the neighbor delay in the routing table
            if(routingType == 2)
            {
                pathRouteInfo Info = {temp_id,myAddress,tempLinkDelay};
                double oldDelay = dist[temp_id].delay;
                dist[temp_id] = Info;
                updateSelfDV(temp_id, tempLinkDelay-oldDelay);
            }
        }
        else
        {
            EV << "We do not find the queue type" << endl << endl << endl << endl;
            temp_id = Queue_NB_MAP.find(i)->second;
            NeighborTable.find(temp_id)->second = 0.001*myAddress;
            EV << "Read neighbor table: " << NeighborTable.find(temp_id)->second << endl;

            if(updateNBTable.find(temp_id) != updateNBTable.end())
            {
                updateNBTable.find(temp_id)->second = 0.001*myAddress;
            }
        }
    }

    EV << "Prediction table:" << endl;
    for(i=0; i<gateNo; i++)
    {
        EV << "Interface: " << i << ", next data arrive time: " << predictTable[i] << endl;
    }

    /*if(TopologyMap.find(myAddress) == TopologyMap.end())
    {
        TopologyMap.insert(std::make_pair(myAddress,updateNBTable));
    }
    else
    {
        TopologyMap.find(myAddress)->second = updateNBTable;
    }*/
    TopologyMap[myAddress] = updateNBTable;
    //TopologyMap[myAddress] = NeighborTable;

    /***********************************************************************/
    // An extreme case that computes path delay each time we have an update
    if(routingType == 1)
    {
        EV << endl << "Content in the UpdateTable is:" << endl;
        std::map<int,double>::iterator UTIt;
        for(UTIt = updateNBTable.begin(); UTIt != updateNBTable.end(); UTIt++)
        {
            EV << "Node: " << UTIt->first << " delay: " << UTIt->second << endl;
        }
        EV << endl;
        computePathDelay();
    }
    else if(routingType == 2)
    {
        showPathDelay();
    }

    EV << endl << endl;
}

void MyStaticRouting::neighborDiscovery()
{
    int gateNo = gateSize("out");
    int i;
    MyUDPPacket *tempPt = NULL;

    char msgName[32];
    sprintf(msgName, "RTND-%d-Data", getId());

    MyUDPPacket *payload = new MyUDPPacket(msgName);
    payload->setByteLength(50);
    payload->setDestinationPort(1);
    payload->setSourcePort(2);
    payload->setSourceAddr(myAddress);
    payload->setDestinationAddr(NBD_ADDR);
    payload->setKind(INIT_HELLO);
    payload->setSeqNo(1);
    payload->setFlowId(flowId);
    payload->setSendTime(simTime().dbl());
    payload->setLastEnqueueTime(0);
    payload->setPktRequirement(100);
    payload->setIsSource(deviceType == SOURCE);
    payload->setIsCenter(deviceType == CENTER);
    payload->setUpStreamDelay(0);
    payload->setDownStreamDelay(0);
    payload->setRealTimeDownStreamDelay(0);

    for(i=0; i<gateNo-1; i++)
    {
        tempPt = payload->dup();
        send(tempPt, "out", i);
    }

    send(payload, "out", i);
    EV << endl << endl;
}

MyUDPPacket* MyStaticRouting::generateUpdatePkt()
{
    char msgName[32];
    sprintf(msgName, "LDU-N%d-Seq%ld", myAddress, sequence);

    MyUDPPacket *payload = new MyUDPPacket(msgName);
    payload->setByteLength(50);
    payload->setDestinationPort(1);
    payload->setSourcePort(2);
    payload->setSourceAddr(myAddress);
    payload->setKind(LINK_DELAY_UPDATE);
    payload->setSeqNo(sequence);
    payload->setFlowId(flowId);
    payload->setSendTime(simTime().dbl());
    payload->setLastEnqueueTime(0);
    payload->setPktRequirement(0.1);
    payload->setTransferNode(myAddress);
    payload->setIsSource(deviceType == SOURCE);
    payload->setIsCenter(deviceType == CENTER);
    payload->setUpStreamDelay(0);
    payload->setDownStreamDelay(0);
    payload->setRealTimeDownStreamDelay(0);

    if(routingType == 1) //Link State
    {
        payload->setDestinationAddr(LINK_STATE_UPDATE_ADDR);
        //payload->getNeighborDelayInfo() = NeighborTable;
        payload->getNeighborDelayInfo() = updateNBTable;
        payload->setByteLength(28 + 12 * updateNBTable.size());
    }
    else if (routingType == 2) // Distance Vector
    {
        payload->setDestinationAddr(DISTANCE_VECTOR_UPDATE_ADDR);
        payload->getNeighborDelayTable() = dist;
    }

    if(idealMeasure)
    {
        payload->setByteLength(0);
    }
    /*std::map<int,double>::iterator it;
    for(it = payload->getNeighborDelayInfo().begin(); it != payload->getNeighborDelayInfo().end(); it++)
    {
        EV << "Node " << it->first << ", delay: " << it->second << endl;
    }*/
    EV << "The generated update packet size is: " << payload->getByteLength() << endl;
    sequence++;

    return payload;
}

void MyStaticRouting::storeNeighbor(cMessage *msg)
{
    MyUDPPacket *pk = check_and_cast<MyUDPPacket *>(msg);

    EV << "This is a neighbor discovery from node " << pk->getSourceAddr() << ", the type is " << pk->getKind() << endl;
    EV << "Msg from gate: " << msg->getArrivalGate()->getIndex() << endl;
    EV << "Gate full name: " << msg->getArrivalGate()->getFullName() << endl;

    NeighborTable.insert(std::pair<int,double>(pk->getSourceAddr(),0));
    Queue_NB_MAP.insert(std::pair<int,int>(msg->getArrivalGate()->getIndex(),pk->getSourceAddr()));
    NB_Queue_MAP.insert(std::pair<int,int>(pk->getSourceAddr(),msg->getArrivalGate()->getIndex()));

    //For distance vector
    if((!pk->getIsCenter() && !pk->getIsSource()) || pk->getIsCenter() == 1)
    {
        pathRouteInfo Info = {pk->getSourceAddr(),myAddress,0};
        dist[pk->getSourceAddr()] = Info;
    }


    //initialize the routing table
    if(dist.find(myAddress) == dist.end())
    {
        pathRouteInfo Info = {myAddress,myAddress,0};
        dist[myAddress] = Info;
    }

    if(pk->getIsSource() == 0)
    {
        updateNBTable.insert(std::pair<int,double>(pk->getSourceAddr(),0));
        if(pk->getIsCenter() == 1)
            Queue_IsCC_MAP.insert(std::make_pair(msg->getArrivalGate()->getIndex(),pk->getIsCenter()));
    }
    else
    {
        Queue_IsSource_MAP.insert(std::make_pair(msg->getArrivalGate()->getIndex(),pk->getIsSource()));
    }

}

void MyStaticRouting::showTable()
{
    EV << "From Neighbor table, neighbors info: " << endl;
    std::map<int,double>::iterator it;
    for (it = NeighborTable.begin(); it != NeighborTable.end(); it++)
    {
        EV << it->first << " delay: " << it->second << " gate: " << NB_Queue_MAP.find(it->first)->second << endl;
    }
    EV << "This is used for update" << endl;
    for (it = updateNBTable.begin(); it != updateNBTable.end(); it++)
    {
        EV << it->first << " delay: " << it->second << " gate: " << NB_Queue_MAP.find(it->first)->second << endl;
    }
    EV << "From Routing table, the gate <-> addr relation is:" << endl;;
    RoutingTable::iterator Rit;
    for (Rit = rtable.begin(); Rit != rtable.end(); Rit++)
    {
        EV << "Addr: " << Rit->first << " from gate: " << Rit->second << endl;
    }
    EV << "The source gate ID is:" << endl;
    std::map<int,bool>::iterator tempIt;
    for(tempIt = Queue_IsSource_MAP.begin(); tempIt != Queue_IsSource_MAP.end(); tempIt++)
    {
        EV << "Gate ID " << tempIt->first << ", is source: " << tempIt->second << endl;
    }

    if(routingType == 2)
    {
        EV << "Initial the DV table:" << endl;
        std::map<int,pathRouteInfo>::iterator initialIt;
        for(initialIt = dist.begin(); initialIt != dist.end(); initialIt++)
        {
            EV << "Node " << initialIt->first <<": " << initialIt->second.delay << "; next Node ID: " << \
                            initialIt->second.nextNode << "; destination previous node: " << initialIt->second.destPreNode <<endl;
        }
    }
}

void MyStaticRouting::sendUpdate()
{
    MyUDPPacket *updatePkt = generateUpdatePkt();
    MyUDPPacket *tempPt = NULL;

    int gateNo = gateSize("out");
    int i;
    int counter = 0;
    int pbCounter = 0;
    double updateTransTime = 0;

    updatePkt->getPbUpdate().generationNode = myAddress;
    updatePkt->getPbUpdate().sequence = updatePkt->getSeqNo();
    updatePkt->getPbUpdate().name.assign(updatePkt->getName());
    EV << "Send update of " << updatePkt->getPbUpdate().name.c_str() << " " << updatePkt->getName() << endl;
    // here we set the transferNode to the neighbor ID to test the correctness of the map
    for(i=0; i<gateNo; i++)
    {
        modulePt = getParentModule()->getSubmodule("queue",i);
        Ptr = dynamic_cast<QueueBase*>(modulePt);
        updateTransTime = Ptr->remainingSrvTime() + \
                (28 + updatePkt->getNeighborDelayInfo().size() * 12) * 8 / Ptr->linkrate;

        EV << "Interface_" << i << ": nextDataTime is " << predictTable[i] << ", measure sent out until " << simTime().dbl() \
                                    + updateTransTime << endl;

        if(Queue_IsSource_MAP.find(i) == Queue_IsSource_MAP.end() && Queue_IsCC_MAP.find(i) == Queue_IsCC_MAP.end())
        {
            updatePkt->setExpectedNeighbor(Queue_NB_MAP.find(i)->second);
            if(usePiggyback)
            {
                if(isPiggybacked(updatePkt,i,routingType))
                {
                    pbCounter++;
                    counter++;
                    continue;
                }
            }
            tempPt = updatePkt->dup();

            EV << "Gate " << i << " generates update" << endl;
            //send(tempPt, "out", i);
            sendDelayed(tempPt, 0, "out", i);
            counter++;
        }
    }

    EV << "Periodic update: send " << counter << " updates, " << pbCounter << " uses piggyback" << endl;
    /*updatePkt->setExpectedNeighbor(Queue_NB_MAP.find(i)->second);
    send(updatePkt, "out", i);*/
    delete updatePkt;
}

void MyStaticRouting::handleLinkDelayUpdate(cMessage *msg)
{
    EV << "This is a link delay update" << endl << endl;

    MyUDPPacket *pkt = dynamic_cast<MyUDPPacket*>(msg);
    int sourceAddr;
    long updateSeq;
    /*double updateTransTime;
    updateTransTime = (28 + pkt->getNeighborDelayInfo().size() * 12) * 8 / 480000.0;*/

    if (pkt)
    {
        if(pkt->getKind() == LINK_DELAY_UPDATE)
        {
            sourceAddr = pkt->getSourceAddr();
            updateSeq = pkt->getSeqNo();
        }
        else if(pkt->getKind() == PIGGY_BACK)
        {
            sourceAddr = pkt->getPbUpdate().generationNode;
            updateSeq = pkt->getPbUpdate().sequence;
        }

        EV << "The update is generated from node " << sourceAddr << ", transfer node " << pkt->getTransferNode() << \
                ", expected neighbor ID: " << pkt->getExpectedNeighbor() << ", my address: " << myAddress << endl;
    }


    if (sourceAddr == myAddress)
    {
        return;
    }



    if(seqRecord.find(sourceAddr) == seqRecord.end())
    {
        seqRecord.insert(std::make_pair(sourceAddr,0));
    }

    if(seqRecord.find(sourceAddr)->second < updateSeq)
    {
        //std::map<int,double> tempMap = pkt->getNeighborDelayInfo();
        seqRecord.find(sourceAddr)->second = updateSeq;
        TopologyMap[sourceAddr] = pkt->getNeighborDelayInfo();

        EV << "Map Size: " << TopologyMap.size() << endl;
        scanTopology();


        /*if(prediction && nextDataTime.dbl() >= 0 && nextDataTime.dbl() < simTime().dbl() + updateTransTime)
        {
            EV << "nextDataTime is " << nextDataTime.dbl() << ", measure sent out until " << simTime().dbl() \
                            + updateTransTime << endl;
            EV << "We should stop the flooding" << endl;
        }
        else*/
        {
            EV << endl << "Flood to other nodes" << endl;
            updateFlood(msg);
        }
        /***********************************************************************/
        // An extreme case that computes path delay each time we have an update
        EV << endl << "Start computation:" << endl;
        computePathDelay();
    }
    else
    {
        EV << "Already have one, discard this update" << endl;
    }
}

void MyStaticRouting::scanTopology()
{
    std::map<int,std::map<int,double> >::iterator tempIt2;
    std::map<int,double>::iterator tempIt;
    for(tempIt2 = TopologyMap.begin(); tempIt2 != TopologyMap.end(); tempIt2++)
    {
        EV << "In function TEST:" << "Node " << tempIt2->first << " neighbors: " << tempIt2->second.size() << endl;
        for(tempIt = tempIt2->second.begin(); tempIt != tempIt2->second.end(); tempIt++)
        {
            EV << "In function MAP11 test: Node " << tempIt->first << ", delay: " << tempIt->second << " " << endl;
        }
    }

    std::map<int,long>::iterator tempIt3;
    for(tempIt3 = seqRecord.begin(); tempIt3 != seqRecord.end(); tempIt3++)
    {
        EV << "Sequence pair: " << tempIt3->first << " <-> " << tempIt3->second << endl;
    }
}

void MyStaticRouting::updateFlood(cMessage *msg)
{
    MyUDPPacket *updatePkt = dynamic_cast<MyUDPPacket*>(msg);
    MyUDPPacket *tempPt = NULL;

    int tNode = updatePkt->getTransferNode();
    int exceptGate = NB_Queue_MAP.find(tNode)->second;
    int counter = 0;
    int pbCounter = 0;
    double updateTransTime = 0;

    EV << "Received from Node: " << tNode << endl;

    int gateNo = gateSize("out");
    int i;
    updatePkt->setTransferNode(myAddress);

    for(i=0; i<gateNo; i++)
    {
        modulePt = getParentModule()->getSubmodule("queue",i);
        Ptr = dynamic_cast<QueueBase*>(modulePt);
        updateTransTime = Ptr->remainingSrvTime() + \
                (28 + updatePkt->getNeighborDelayInfo().size() * 12) * 8 / Ptr->linkrate;

        EV << "Interface_" << i << ": nextDataTime is " << predictTable[i] << ", measure sent out until " << simTime().dbl() \
                                    + updateTransTime << endl;

        if(i != exceptGate && (Queue_IsSource_MAP.find(i) == Queue_IsSource_MAP.end())\
                && (Queue_IsCC_MAP.find(i) == Queue_IsCC_MAP.end()))
        {
            if(!prediction || predictTable[i] < 0 || predictTable[i] > simTime().dbl() + updateTransTime)
            {
            updatePkt->setExpectedNeighbor(Queue_NB_MAP.find(i)->second);

            if(usePiggyback)
            {
                if(isPiggybacked(msg,i,routingType))
                {
                    pbCounter++;
                    counter++;
                    continue;
                }
            }

            tempPt = updatePkt->dup();
            tempPt->setKind(LINK_DELAY_UPDATE);
            tempPt->setByteLength(28 + 12 * tempPt->getNeighborDelayInfo().size());
            tempPt->setName(tempPt->getPbUpdate().name.c_str());
            tempPt->setSeqNo(tempPt->getPbUpdate().sequence);
            tempPt->setSourceAddr(tempPt->getPbUpdate().generationNode);
            tempPt->setDestinationAddr(LINK_STATE_UPDATE_ADDR);
            tempPt->setFlowId(tempPt->getPbUpdate().generationNode);
            tempPt->setSendTime(simTime().dbl());
            tempPt->setLastEnqueueTime(0);
            tempPt->setPktRequirement(0.1);
            tempPt->setIsSource(false);

            if(idealMeasure)
            {
                tempPt->setByteLength(0);
            }


            EV << "Flooding to gate " << i << " generates new update, packet size is: " << tempPt->getByteLength() << endl;
            send(tempPt, "out", i);
            counter++;
            }
        }
    }

    EV << "Flooding completed, send " << counter << " updates, " << pbCounter << " uses piggyback" << endl;
}

void MyStaticRouting::computePathDelay()
{
    //double dist[routerNumber];
    //bool s[routerNumber];
    unsigned int counter = 0;
    int currentNode;
    double minDist;
    double tempVar;
    int nextNode;
    std::map<int,double>::iterator tempIt;
    std::map<int,pathRouteInfo>::iterator routeInfoIt;
    std::map<int,bool>::iterator boolIt;
    pathRouteInfo initialInfo = {-1,-2,INFINITY_DISTANCE};
    std::map<int,std::map<int,double> >::iterator topoIt;

    // initialize the distance table

    /*for(topoIt = TopologyMap.begin(); topoIt != TopologyMap.end(); topoIt++)
    {
        if(topoIt->first == myAddress)
        {
            dist[topoIt->first] = 0;
            nextHopTable[topoIt->first] = topoIt->first;
        }
        else
        {
            dist[topoIt->first] = INFINITY_DISTANCE;
        }

        sContainer[topoIt->first] = 0;
    }*/

    /*for(int i = 0; i != routerNumber; i++)
    {
        if(i == myAddress)
        {
            dist[i] = 0;
            nextHopTable[i] = i;
        }
        else
        {
            dist[i] = INFINITY_DISTANCE;
        }

        sContainer[i] = 0;
    }*/

    /*for(int i = 0; i != routerNumber; i++)
    {
        if(i == myAddress)
        {
            dist[i] = initialInfo;
            dist[i].nextNode = myAddress;
            dist[i].delay = 0;
            dist[i].destPreNode = myAddress;
        }
        else
        {
            dist[i] = initialInfo;
        }

        sContainer[i] = 0;
    }*/

    for(topoIt = TopologyMap.begin(); topoIt != TopologyMap.end(); topoIt++)
    {
        for(tempIt = topoIt->second.begin(); tempIt != topoIt->second.end(); tempIt++)
        {
            if(tempIt->first == myAddress)
            {
                dist[tempIt->first] = initialInfo;
                dist[tempIt->first].nextNode = myAddress;
                dist[tempIt->first].delay = 0;
                dist[tempIt->first].destPreNode = myAddress;
            }
            else
            {
                dist[tempIt->first] = initialInfo;
            }

            sContainer[tempIt->first] = 0;
        }
    }

    sContainer[myAddress] = 1; // start from the node itself
    counter++;
    currentNode = myAddress;

    EV << "Start computing path, topology size is "<< TopologyMap.size() << endl;

    //while(counter != routerNumber)
    while(counter != dist.size())
    {
        minDist = INFINITY_DISTANCE;
        if(TopologyMap.find(currentNode) != TopologyMap.end())
        {
            for(tempIt = TopologyMap.find(currentNode)->second.begin(); \
            tempIt != TopologyMap.find(currentNode)->second.end(); tempIt++)
            {
                tempVar = dist[currentNode].delay + tempIt->second;
                if( !sContainer[tempIt->first] && tempVar < dist[tempIt->first].delay)
                {
                    dist[tempIt->first].delay = tempVar;

                    if(currentNode == myAddress)
                    {
                        dist[tempIt->first].nextNode = tempIt->first;
                    }
                    else
                    {
                        dist[tempIt->first].nextNode = dist[currentNode].nextNode;
                    }
                    dist[tempIt->first].destPreNode = currentNode;
                }
            }
        }

        for(routeInfoIt = dist.begin(); routeInfoIt != dist.end(); routeInfoIt++)
        {
            if(!sContainer[routeInfoIt->first] && dist[routeInfoIt->first].delay <= minDist)
            {
                nextNode = routeInfoIt->first;
                minDist = dist[routeInfoIt->first].delay;
            }
        }


        sContainer[nextNode] = 1;
        currentNode = nextNode;
        counter++;
    }

    for(boolIt = sContainer.begin(); boolIt != sContainer.end(); boolIt++)
    {
        EV << boolIt->second;
    }
    EV << endl;
    showPathDelay();
}

void MyStaticRouting::showPathDelay()
{
    //std::map<int,double>::iterator tempIt;
    //std::map<int,int>::iterator tempIt2;
    std::map<int,pathRouteInfo>::iterator tempIt;

    EV << "The delay to other nodes are:" << endl;
    for(tempIt = dist.begin(); tempIt != dist.end(); tempIt++)
    {
        EV << "Node " << tempIt->first <<": " << tempIt->second.delay << "; next Node ID: " << \
                tempIt->second.nextNode << "; destination previous node: " << tempIt->second.destPreNode <<endl;
    }

    /*EV << "Showing previous node" << endl;
    for(tempIt2 = preNodeTable.begin(); tempIt2 != preNodeTable.end(); tempIt2++)
    {
        EV << "Node pairs: " << tempIt2->first << " : " << tempIt2->second << endl;
    }*/
}

void MyStaticRouting::recordDelay(cMessage *msg)
{
    MyUDPPacket *pk = check_and_cast<MyUDPPacket *>(msg);
    int destination = pk->getDestinationAddr();
    double dsDelay = 0;

    if(!multipaths)
    {
        dsDelay = dist.find(destination)->second.delay;
    }
    else
    {
        int dst = pk->getDestinationAddr();
        int currentAddr = myAddress;
        int next = staticRtRecPt->rtRecord.find(currentAddr)->second.find(dst)->second;
        //nextNodeID = next;
        EV << "The path is: ";
        while(next != dst)
        {
            EV << currentAddr << " ";
            dsDelay += TopologyMap.find(currentAddr)->second.find(next)->second;
            currentAddr = next;
            next = staticRtRecPt->rtRecord.find(currentAddr)->second.find(dst)->second;
        }
        EV << currentAddr << endl;
        dsDelay += TopologyMap.find(currentAddr)->second.find(next)->second;
    }

    EV << "From the table, delay is: " << dist.find(destination)->second.delay << ", while in static record, delay is " <<\
            dsDelay << endl;

    testDelayRpt rptVar = {myAddress, dsDelay, simTime().dbl()};

    pk->getRptVector().push_back(rptVar);
}

void MyStaticRouting::lookUpDownStreamDelay(cMessage *msg)
{
    double dsDelay = 0;
    int nextNodeID;
    MyUDPPacket *pk = check_and_cast<MyUDPPacket *>(msg);

    if(!multipaths)
    {
        double delay1 = dist.find(pk->getDestinationAddr())->second.delay;
        nextNodeID = dist.find(pk->getDestinationAddr())->second.nextNode;
        double delay2 = dist.find(nextNodeID)->second.delay;

        dsDelay = delay1 - delay2;
    }
    else
    {
        int dst = pk->getDestinationAddr();
        int currentAddr = myAddress;
        int next = staticRtRecPt->rtRecord.find(currentAddr)->second.find(dst)->second;
        nextNodeID = next;
        EV << "The path is: ";
        while(next != dst)
        {
            EV << currentAddr << " ";
            dsDelay += TopologyMap.find(currentAddr)->second.find(next)->second;
            currentAddr = next;
            next = staticRtRecPt->rtRecord.find(currentAddr)->second.find(dst)->second;
        }
        EV << currentAddr << endl;
        dsDelay += TopologyMap.find(currentAddr)->second.find(next)->second;
        dsDelay -= TopologyMap.find(myAddress)->second.find(nextNodeID)->second;
    }

    pk->setRealTimeDownStreamDelay(dsDelay);
    pk->getDownStreamSeen().push_back(dsDelay);

    EV << "To " << pk->getDestinationAddr() << ", via " << nextNodeID << ", DD is " << dsDelay << endl;
}

void MyStaticRouting::handleDVUpdate(cMessage *msg)
{
    MyUDPPacket *pkt = dynamic_cast<MyUDPPacket*>(msg);
    int sourceAddr;
    long updateSeq;

    if (pkt)
    {
        if(pkt->getKind() == LINK_DELAY_UPDATE)
        {
            sourceAddr = pkt->getSourceAddr();
            updateSeq = pkt->getSeqNo();
        }
        else if(pkt->getKind() == PIGGY_BACK)
        {
            sourceAddr = pkt->getPbUpdate().generationNode;
            updateSeq = pkt->getPbUpdate().sequence;
        }

        EV << "The update is generated from node " << sourceAddr << ", transfer node " << pkt->getTransferNode() << \
                ", expected neighbor ID: " << pkt->getExpectedNeighbor() << ", my address: " << myAddress << endl;
    }


    if (sourceAddr == myAddress)
    {
        return;
    }


    if(seqRecord.find(sourceAddr) == seqRecord.end())
    {
        seqRecord.insert(std::make_pair(sourceAddr,updateSeq));
        computePathDelayDV(msg);
    }
    else if(seqRecord.find(sourceAddr)->second < updateSeq)
    {
        seqRecord.find(sourceAddr)->second = updateSeq;
        computePathDelayDV(msg);
    }
    else
    {
        EV << "Already have one, discard this update" << endl;
    }

    showPathDelay();
}

void MyStaticRouting::updateSelfDV(int nextID, double delta_delay)
{
    std::map<int,pathRouteInfo>::iterator tempIt;

    for(tempIt = dist.begin(); tempIt != dist.end(); tempIt++)
    {
        if(tempIt->first != nextID && tempIt->second.nextNode == nextID)
        {
            tempIt->second.delay += delta_delay;
        }
    }
}

void MyStaticRouting::computePathDelayDV(cMessage *msg)
{
    MyUDPPacket *pkt = dynamic_cast<MyUDPPacket*>(msg);
    std::map<int,pathRouteInfo> tempDVMap = pkt->getNeighborDelayTable();
    std::map<int,pathRouteInfo>::iterator tempIt;
    bool updatePreNode;
    int sourceAddr;

    sourceAddr = pkt->getSourceAddr();

    for(tempIt = tempDVMap.begin(); tempIt != tempDVMap.end(); tempIt++)
    {
        updatePreNode = true;
        if(dist.find(tempIt->first) == dist.end())
        {
            pathRouteInfo tempPathInfo = {sourceAddr,tempIt->second.destPreNode,\
                    (tempIt->second.delay + dist.find(sourceAddr)->second.delay)};
            dist[tempIt->first] = tempPathInfo;
        }
        else if(tempIt->second.nextNode == myAddress)
        {
            updatePreNode = false;
            EV << "Do not update to node " << tempIt->first << " since the route is through ourselves" << endl;
        }
        else if(dist.find(tempIt->first)->second.nextNode == sourceAddr)
        {
            dist.find(tempIt->first)->second.delay = dist.find(sourceAddr)->second.delay + tempIt->second.delay;
            dist.find(tempIt->first)->second.destPreNode = tempIt->second.destPreNode;
        }
        else
        {
            double pathDelay = dist.find(sourceAddr)->second.delay + tempIt->second.delay;
            if(pathDelay < dist.find(tempIt->first)->second.delay)
            {
                dist.find(tempIt->first)->second.delay = pathDelay;
                dist.find(tempIt->first)->second.nextNode = sourceAddr;
                dist.find(tempIt->first)->second.destPreNode = tempIt->second.destPreNode;
            }
            else
            {
                updatePreNode = false;
                EV << "New delay to node " << tempIt->first << " is " << pathDelay << " , not smaller than the old one " << \
                        dist.find(tempIt->first)->second.delay << endl;
            }
        }

        if(updatePreNode)
        {
            if(NeighborTable.find(tempIt->first) != NeighborTable.end())
            {
                dist.find(tempIt->first)->second.destPreNode = myAddress;
            }
        }
    }
}

bool MyStaticRouting::isPiggybacked(cMessage *msg, int gateID, int updateType)
{
    cModule *modulePtr = getParentModule()->getSubmodule("queue",gateID);
    QueueBase *queuePtr = dynamic_cast<QueueBase*>(modulePtr);
    bool result;

    if(queuePtr)
    {
        result = queuePtr->doPiggyback(msg,updateType);
    }
    return result;
}

void MyStaticRouting::staticRtRec()
{
    cModule *modulePt = NULL;
    modulePt = getParentModule();

    while(modulePt != NULL)
    {
        if(modulePt->getSubmodule("StaticRoutingRecord") != NULL)
        {
            EV << "Find the module" << endl;
            staticRtRecPt = dynamic_cast<MyStaticRtRec*>(modulePt->getSubmodule("StaticRoutingRecord"));
            if(staticRtRecPt != NULL)
            {
                EV << "Successfully find the pointer to the StaticRoutingRecord module" << endl;
                staticRtRecPt->sayHello();
            }
            break;
        }
        modulePt = modulePt->getParentModule();
    }

    if(modulePt == NULL)
    {
        EV << "No such module" << endl;
    }
}
