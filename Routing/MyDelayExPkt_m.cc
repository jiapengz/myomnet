//
// Generated file, do not edit! Created by opp_msgc 4.4 from Routing/MyDelayExPkt.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#include <iostream>
#include <sstream>
#include "MyDelayExPkt_m.h"

USING_NAMESPACE

// Template rule which fires if a struct or class doesn't have operator<<
template<typename T>
std::ostream& operator<<(std::ostream& out,const T&) {return out;}

// Another default rule (prevents compiler from choosing base class' doPacking())
template<typename T>
void doPacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doPacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}

template<typename T>
void doUnpacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doUnpacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}




Register_Class(MyDelayExPkt);

MyDelayExPkt::MyDelayExPkt(const char *name, int kind) : ::MyUDPPacket(name,kind)
{
    this->transferNodeLS_var = 0;
    this->expectedNeighborLS_var = 0;
}

MyDelayExPkt::MyDelayExPkt(const MyDelayExPkt& other) : ::MyUDPPacket(other)
{
    copy(other);
}

MyDelayExPkt::~MyDelayExPkt()
{
}

MyDelayExPkt& MyDelayExPkt::operator=(const MyDelayExPkt& other)
{
    if (this==&other) return *this;
    ::MyUDPPacket::operator=(other);
    copy(other);
    return *this;
}

void MyDelayExPkt::copy(const MyDelayExPkt& other)
{
    this->transferNodeLS_var = other.transferNodeLS_var;
    this->expectedNeighborLS_var = other.expectedNeighborLS_var;
    this->neighborDelayInfoLS_var = other.neighborDelayInfoLS_var;
}

void MyDelayExPkt::parsimPack(cCommBuffer *b)
{
    ::MyUDPPacket::parsimPack(b);
    doPacking(b,this->transferNodeLS_var);
    doPacking(b,this->expectedNeighborLS_var);
    doPacking(b,this->neighborDelayInfoLS_var);
}

void MyDelayExPkt::parsimUnpack(cCommBuffer *b)
{
    ::MyUDPPacket::parsimUnpack(b);
    doUnpacking(b,this->transferNodeLS_var);
    doUnpacking(b,this->expectedNeighborLS_var);
    doUnpacking(b,this->neighborDelayInfoLS_var);
}

int MyDelayExPkt::getTransferNodeLS() const
{
    return transferNodeLS_var;
}

void MyDelayExPkt::setTransferNodeLS(int transferNodeLS)
{
    this->transferNodeLS_var = transferNodeLS;
}

int MyDelayExPkt::getExpectedNeighborLS() const
{
    return expectedNeighborLS_var;
}

void MyDelayExPkt::setExpectedNeighborLS(int expectedNeighborLS)
{
    this->expectedNeighborLS_var = expectedNeighborLS;
}

DelayMap& MyDelayExPkt::getNeighborDelayInfoLS()
{
    return neighborDelayInfoLS_var;
}

void MyDelayExPkt::setNeighborDelayInfoLS(const DelayMap& neighborDelayInfoLS)
{
    this->neighborDelayInfoLS_var = neighborDelayInfoLS;
}

class MyDelayExPktDescriptor : public cClassDescriptor
{
  public:
    MyDelayExPktDescriptor();
    virtual ~MyDelayExPktDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(MyDelayExPktDescriptor);

MyDelayExPktDescriptor::MyDelayExPktDescriptor() : cClassDescriptor("MyDelayExPkt", "MyUDPPacket")
{
}

MyDelayExPktDescriptor::~MyDelayExPktDescriptor()
{
}

bool MyDelayExPktDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<MyDelayExPkt *>(obj)!=NULL;
}

const char *MyDelayExPktDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int MyDelayExPktDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 3+basedesc->getFieldCount(object) : 3;
}

unsigned int MyDelayExPktDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISCOMPOUND,
    };
    return (field>=0 && field<3) ? fieldTypeFlags[field] : 0;
}

const char *MyDelayExPktDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "transferNodeLS",
        "expectedNeighborLS",
        "neighborDelayInfoLS",
    };
    return (field>=0 && field<3) ? fieldNames[field] : NULL;
}

int MyDelayExPktDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='t' && strcmp(fieldName, "transferNodeLS")==0) return base+0;
    if (fieldName[0]=='e' && strcmp(fieldName, "expectedNeighborLS")==0) return base+1;
    if (fieldName[0]=='n' && strcmp(fieldName, "neighborDelayInfoLS")==0) return base+2;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *MyDelayExPktDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "int",
        "DelayMap",
    };
    return (field>=0 && field<3) ? fieldTypeStrings[field] : NULL;
}

const char *MyDelayExPktDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int MyDelayExPktDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    MyDelayExPkt *pp = (MyDelayExPkt *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string MyDelayExPktDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    MyDelayExPkt *pp = (MyDelayExPkt *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getTransferNodeLS());
        case 1: return long2string(pp->getExpectedNeighborLS());
        case 2: {std::stringstream out; out << pp->getNeighborDelayInfoLS(); return out.str();}
        default: return "";
    }
}

bool MyDelayExPktDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    MyDelayExPkt *pp = (MyDelayExPkt *)object; (void)pp;
    switch (field) {
        case 0: pp->setTransferNodeLS(string2long(value)); return true;
        case 1: pp->setExpectedNeighborLS(string2long(value)); return true;
        default: return false;
    }
}

const char *MyDelayExPktDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
        "DelayMap",
    };
    return (field>=0 && field<3) ? fieldStructNames[field] : NULL;
}

void *MyDelayExPktDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    MyDelayExPkt *pp = (MyDelayExPkt *)object; (void)pp;
    switch (field) {
        case 2: return (void *)(&pp->getNeighborDelayInfoLS()); break;
        default: return NULL;
    }
}


