/*
 * SMSQueue.h
 *
 *  Created on: May 12, 2014
 *      Author: Zhang Jiapeng
 */

#ifndef SMSQUEUE_H_
#define SMSQUEUE_H_

#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
#include "cSMSQueue.h"
#include "sigListener.h"
#include "QueueBase.h"

class SMSQueue : public QueueBase
{
  private:
    long frameCapacity;
    //double linkrate;
    //double linkdelay;
    int type;
    bool useSlack;
    bool pmuFirst;

    //cDatarateChannel *channel;
    cModule *assistMod;

    cSMSQueue queue;
    cMessage *endTransmissionEvent;
    cMessage *periodCompute; //periodically compute the mean delay

    simtime_t startTime;
    simtime_t reportPeriod;
    simtime_t stopTime;

    /*simsignal_t qlenSignal;
    simsignal_t busySignal;
    simsignal_t queueingTimeSignal;
    simsignal_t dropSignal;
    simsignal_t txBytesSignal;
    simsignal_t rxBytesSignal;*/

    simsignal_t packetState;
    cIListener *listener;

  public:
    //double total_delay; //sum of the link delay for all packets within a period
    //double mean_delay; //used for the link delay measurement
    MyUDPPacket *myPkt;
    double temp_var;
    //unsigned int total_pkt_no;

    bool DoRecord;

  public:
    SMSQueue();
    virtual ~SMSQueue();
    virtual void delayCompute();

  protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);

    virtual void startTransmitting(cMessage *msg);

    virtual void displayStatus(bool isBusy);
    virtual bool doPiggyback(cMessage *msg, int updateType);
    virtual double remainingSrvTime();

};




#endif /* SMSQUEUE_H_ */
