//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef QUEUEBASE_H_
#define QUEUEBASE_H_

#include <omnetpp.h>
#include "SimType.h"

class QueueBase : public cSimpleModule
{
  public:
    double linkrate;
    double linkdelay;
    double total_delay; //sum of the link delay for all packets within a period
    double mean_delay; //used for the link delay measurement
    unsigned int total_pkt_no; //total received pkt number within a period

    cDatarateChannel *channel; //connected channel
    simtime_t expectedTransEnd; //The finish time of current packet

  public:
    QueueBase();
    virtual ~QueueBase();
    virtual void delayCompute();
    virtual bool doPiggyback(cMessage *msg, int updateType);
    virtual double remainingSrvTime();
};

#endif /* QUEUEBASE_H_ */
