//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 1992-2008 Andras Varga
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//



/**
 * Used for finding the 1st place in SMS.
 */

/*int findFirstPlace(cObject *newPacket, cObject *oldPacket)
{
    MyUDPPacket *newpkt_t = dynamic_cast<MyUDPPacket*>(newPacket);
    MyUDPPacket *oldpkt_t = dynamic_cast<MyUDPPacket*>(oldPacket);

    if(!newpkt_t)
        return 1;
    if(!oldpkt_t)
        return 1;

    if(newpkt_t->getFlowId() == oldpkt_t->getFlowId())
        return 1;

    double dt_new, dt_old;
    dt_new = newpkt_t->getPktRequirement() - (simTime().dbl() - newpkt_t->getSendTime()) - newpkt_t->getDownStreamDelay()\
            - newpkt_t->getUpStreamDelay();
    dt_old = oldpkt_t->getPktRequirement() - (simTime().dbl() - oldpkt_t->getSendTime()) - oldpkt_t->getDownStreamDelay()\
            - oldpkt_t->getUpStreamDelay();

    if (dt_new - dt_old < 0)
        return -1;
    else
        return 1;
}*/

/**
 * Point-to-point interface module. While one frame is transmitted,
 * additional frames get queued up; see NED file for more info.
 */

#include "SMSQueue.h"

Define_Module(SMSQueue);

SMSQueue::SMSQueue()
{
    endTransmissionEvent = NULL;
    periodCompute = NULL;
}

SMSQueue::~SMSQueue()
{
    cancelAndDelete(endTransmissionEvent);
    cancelAndDelete(periodCompute);
}

void SMSQueue::initialize()
{
    channel = dynamic_cast<cDatarateChannel*>(gate("line$o")->getTransmissionChannel());
    if (channel)
    {
        linkrate = channel->getDatarate();
        linkdelay = channel->getDelay().dbl();
    }

    useSlack = par("useSlack").boolValue();
    type = par("qType");
    pmuFirst = par("pmuFirst").boolValue();

    queue.setName("smsQueue");
    //queue.init();
    queue.setUseSlack(useSlack);
    queue.setQueueType(type);
    queue.setLinkRate(linkrate);
    queue.setTrafficPriority(pmuFirst);

    endTransmissionEvent = new cMessage("endTxEvent");
    periodCompute = new cMessage("meanRptEvent");
    startTime = par("SMSQStartTime").doubleValue();
    reportPeriod = par("SMSQReportPeriod").doubleValue();
    stopTime = par("SMSQStopTime").doubleValue();

    frameCapacity = par("frameCapacity");

    EV << "SMSQueue: linkrate: " << linkrate << ", linkdelay: " << linkdelay << endl;
    EV << "useSlack: " << useSlack << ", queue type: " << type << endl;

    assistMod = getParentModule()->getSubmodule("assistMod");
    if (assistMod)
    {
        EV << "Assist Module found" << endl;
    }
    else
    {
        EV << "No Assist Modules" << endl;
    }

    AssistModule *amptr = dynamic_cast<AssistModule*>(assistMod);
    if (amptr)
    {
        EV << "Successful transfer" << endl;
        queue.setAssistModPtr(amptr);
    }

    EV << "Now we are calling from the SMS queue" << endl;
    queue.getAssisModPtr()->mapScan();

    //initialize the measurement variable
    total_delay = 0;
    mean_delay = 0;
    myPkt = NULL;
    temp_var = 0;
    total_pkt_no = 0;

    /*qlenSignal = registerSignal("qlen");
    busySignal = registerSignal("busy");
    queueingTimeSignal = registerSignal("queueingTime");
    dropSignal = registerSignal("drop");
    txBytesSignal = registerSignal("txBytes");
    rxBytesSignal = registerSignal("rxBytes");

    emit(qlenSignal, queue.length());
    emit(busySignal, false);*/

    DoRecord = par("record").boolValue();

    char filename[40];
    int nodeId = getParentModule()->par("address");
    sprintf(filename,"MyRecResult\\%s-N%d-%d","MyRecord",nodeId,getIndex());
    packetState = registerSignal("pktState");
    //listener = new sigListener(filename);
    listener = new sigListener();
    subscribe(packetState,listener);

    //schedule computing the mean delay of the link
    if(startTime > SIMTIME_ZERO && reportPeriod >SIMTIME_ZERO)
    {
        scheduleAt((startTime + reportPeriod), periodCompute);
    }
}

void SMSQueue::startTransmitting(cMessage *msg)
{
    if (ev.isGUI()) displayStatus(true);

    EV << "Starting transmission of " << msg << endl;
    expectedTransEnd = simTime() + myPkt->getBitLength() / linkrate;
    myPkt = dynamic_cast<MyUDPPacket*>(msg);
    if(myPkt && myPkt->getKind() != LINK_DELAY_UPDATE)
    {
        total_pkt_no++;
        temp_var = simTime().dbl() - myPkt->getLastEnqueueTime() + myPkt->getBitLength() / linkrate;
        total_delay += temp_var;
        EV << "At time " << simTime().dbl() << ", enqueue at " << myPkt->getLastEnqueueTime() << ", delay at the link is: " \
                << temp_var << ", total delay: " << total_delay << ", total No.: " << total_pkt_no << endl << endl;
    }
    //int64 numBytes = check_and_cast<cPacket *>(msg)->getByteLength();
    queue.setSrvStartTime(simTime());
    send(msg, "line$o");

    //emit(txBytesSignal, (long)numBytes);

    // Schedule an event for the time when last bit will leave the gate.
    simtime_t endTransmission = gate("line$o")->getTransmissionChannel()->getTransmissionFinishTime();
    scheduleAt(endTransmission, endTransmissionEvent);
}

void SMSQueue::handleMessage(cMessage *msg)
{
    if (msg==endTransmissionEvent)
    {
        // Transmission finished, we can start next one.
        EV << "Transmission finished.\n";
        if (ev.isGUI()) displayStatus(false);
        if (queue.empty())
        {
            //emit(busySignal, false);
            EV << "Empty queue" << endl;
        }
        else
        {
            msg = (cMessage *) queue.pop();
            //emit(queueingTimeSignal, simTime() - msg->getTimestamp());
            //emit(qlenSignal, queue.length());
            //emit(packetState, msg);
            startTransmitting(msg);
        }
    }
    else if (msg == periodCompute)
    {
        delayCompute();
        if(simTime() + reportPeriod <= stopTime)
        {
            scheduleAt((simTime() + reportPeriod),periodCompute);
        }
    }
    else if (msg->arrivedOn("line$i"))
    {
        // pass up to routing module
        //emit(rxBytesSignal, (long)check_and_cast<cPacket *>(msg)->getByteLength());
        send(msg,"out");
    }
    else // arrived on gate "in"
    {
        myPkt = dynamic_cast<MyUDPPacket*>(msg);
        if(myPkt)
        {
            myPkt->setLastEnqueueTime(simTime().dbl());
            EV << "Current Time: " << myPkt->getLastEnqueueTime() << endl << endl;
        }

        if (endTransmissionEvent->isScheduled())
        {
            // We are currently busy, so just queue up the packet.
            if (frameCapacity && queue.length()>=frameCapacity)
            {
                EV << "Received " << msg << " but transmitter busy and queue full: discarding\n";
                //emit(dropSignal, (long)check_and_cast<cPacket *>(msg)->getByteLength());
                delete msg;
            }
            else
            {
                EV << "Received " << msg << " but transmitter busy: queueing up\n";
                msg->setTimestamp();
                queue.insert(msg);
                //emit(qlenSignal, queue.length());
            }
        }
        else
        {
            // We are idle, so we can start transmitting right away.
            EV << "Received " << msg << endl;
            //emit(queueingTimeSignal, SIMTIME_ZERO);
            //emit(packetState, msg);
            startTransmitting(msg);
            //emit(busySignal, true);
        }
    }
}

void SMSQueue::displayStatus(bool isBusy)
{
    getDisplayString().setTagArg("t",0, isBusy ? "transmitting" : "idle");
    getDisplayString().setTagArg("i",1, isBusy ? (queue.length()>=3 ? "red" : "yellow") : "");
}

void SMSQueue::delayCompute()
{
    EV << "Function called in class SMSQueue" << endl;
    if(total_pkt_no == 0)
    {
        mean_delay = 0;
    }
    else
    {
        mean_delay = total_delay / total_pkt_no;
    }

    EV << "At time " << simTime().dbl() <<  ", total packet delay is: " << total_delay << ", total pkt no:" << total_pkt_no \
                    << ", mean is:" << mean_delay << endl;

    total_pkt_no = 0;
    total_delay = 0;
}

bool SMSQueue::doPiggyback(cMessage *msg, int updateType)
{
    bool result;

    EV << "Piggyback from the SMSQueue" << endl;
    result = queue.insertPiggyback(msg,updateType);

    return result;
}

double SMSQueue::remainingSrvTime()
{
    EV << "This is the remainingSrvTime in the SMSQueue" << endl;
    double result = 0;
    if(queue.getBitLength() > 0)
    {
        result = queue.getBitLength() / linkrate + expectedTransEnd.dbl() - simTime().dbl();
    }
    else if(expectedTransEnd > simTime())
    {
        result = expectedTransEnd.dbl() - simTime().dbl();
    }

    EV << "Queue length: " << queue.getBitLength() << ", current remaining time: " << expectedTransEnd.dbl() - simTime().dbl()\
            << ", result = " << result << endl;

    return result;
}
