//=========================================================================
//  CQUEUE.CC - part of
//
//                  OMNeT++/OMNEST
//           Discrete System Simulation in C++
//
//   Member functions of
//    cQueueBase : queue of cObject descendants
//
//  Author: Andras Varga
//
//=========================================================================

/*--------------------------------------------------------------*
  Copyright (C) 1992-2008 Andras Varga
  Copyright (C) 2006-2008 OpenSim Ltd.

  This file is distributed WITHOUT ANY WARRANTY. See the file
  `license' for details on this and other legal matters.
*--------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <sstream>
#include "globals.h"
#include "cqueueBase.h"
#include "cexception.h"

#ifdef WITH_PARSIM
#include "ccommbuffer.h"
#endif

NAMESPACE_BEGIN

using std::ostream;


Register_Class(cQueueBase);


cQueueBase::cQueueBase(const cQueueBase& queue) : cOwnedObject(queue)
{
    frontp = backp = NULL;
    n = 0;
    linkrate = 0;
    bitlength = 0;
    copy(queue);
}

cQueueBase::cQueueBase(const char *name, MyCompareFunc cmp) : cOwnedObject(name)
{
    tkownership = true;
    frontp = backp = NULL;
    n = 0;
    linkrate = 0;
    bitlength = 0;
    compare = cmp;
}

cQueueBase::~cQueueBase()
{
    clear();
}

std::string cQueueBase::info() const
{
    if (n==0)
        return std::string("empty");
    std::stringstream out;
    out << "length=" << n;
    return out.str();
}

void cQueueBase::forEachChild(cVisitor *v)
{
    // loop through elements
    for (QElem *p=frontp; p!=NULL; p=p->next)
         v->visit(p->obj);
}

void cQueueBase::parsimPack(cCommBuffer *buffer)
{
#ifndef WITH_PARSIM
    throw cRuntimeError(this,eNOPARSIM);
#else
    cOwnedObject::parsimPack(buffer);

    if (compare)
        throw cRuntimeError(this,"parsimPack(): cannot transmit comparison function");

    buffer->pack(n);

    for (cQueueBase::Iterator iter(*this, 0); !iter.end(); iter--)
    {
        if (iter()->isOwnedObject() && iter()->getOwner() != this)
            throw cRuntimeError(this,"parsimPack(): refusing to transmit an object not owned by the queue");
        buffer->packObject(iter());
    }
#endif
}

void cQueueBase::parsimUnpack(cCommBuffer *buffer)
{
#ifndef WITH_PARSIM
    throw cRuntimeError(this,eNOPARSIM);
#else
    cOwnedObject::parsimUnpack(buffer);

    buffer->unpack(n);

    CompareFunc old_cmp = compare;
    compare = NULL; // temporarily, so that insert() keeps the original order
    for (int i=0; i<n; i++)
    {
        cObject *obj = buffer->unpackObject();
        insert(obj);
    }
    compare = old_cmp;
#endif
}

void cQueueBase::clear()
{
    while (frontp)
    {
        QElem *tmp = frontp->next;
        cObject *obj = frontp->obj;
        if (!obj->isOwnedObject())
            delete obj;
        else if (obj->getOwner()==this)
            dropAndDelete(static_cast<cOwnedObject *>(obj));
        delete frontp;
        frontp = tmp;
    }
    backp = NULL;
    n = 0;
}

void cQueueBase::copy(const cQueueBase& queue)
{
    compare = NULL; // temporarily, so that insert() keeps the original order
    for (cQueueBase::Iterator iter(queue, false); !iter.end(); iter++)
    {
        cObject *obj = iter();
        if (!obj->isOwnedObject())
            {insert(obj->dup());}
        else if (obj->getOwner()==const_cast<cQueueBase*>(&queue))
            {setTakeOwnership(true); insert(obj->dup());}
        else
            {setTakeOwnership(false); insert(obj);}
    }

    tkownership = queue.tkownership;
    compare = queue.compare;
}

cQueueBase& cQueueBase::operator=(const cQueueBase& queue)
{
    if (this==&queue) return *this;
    clear();
    cOwnedObject::operator=(queue);
    copy(queue);
    return *this;
}

void cQueueBase::setup(CompareFunc cmp)
{
    compare = cmp;
}

cQueueBase::QElem *cQueueBase::find_qelem(cObject *obj) const
{
    QElem *p = frontp;
    while (p && p->obj!=obj)
        p = p->next;
    return p;
}

void cQueueBase::insbefore_qelem(QElem *p, cObject *obj)
{
    QElem *e = new QElem;
    e->obj = obj;

    e->prev = p->prev;
    e->next = p;
    p->prev = e;
    if (e->prev)
        e->prev->next = e;
    else
        frontp = e;
    n++;
}

void cQueueBase::insafter_qelem(QElem *p, cObject *obj)
{
    QElem *e = new QElem;
    e->obj = obj;

    e->next = p->next;
    e->prev = p;
    p->next = e;
    if (e->next)
        e->next->prev = e;
    else
        backp = e;
    n++;
}

cObject *cQueueBase::remove_qelem(QElem *p)
{
    if (p->next)
        p->next->prev = p->prev;
    else
        backp = p->prev;
    if (p->prev)
        p->prev->next = p->next;
    else
        frontp = p->next;

    cObject *retobj = p->obj;
    delete p;
    n--;
    if (retobj->isOwnedObject() && retobj->getOwner()==this)
        drop(static_cast<cOwnedObject *>(retobj));

    cPacket *pkt = dynamic_cast<cPacket*>(retobj);
    if (pkt)
    {
        bitlength -= pkt->getBitLength();
    }

    return retobj;
}


void cQueueBase::insert(cObject *obj)
{
    if (!obj)
        throw cRuntimeError(this, "cannot insert NULL pointer");

    if (obj->isOwnedObject() && getTakeOwnership())
        take(static_cast<cOwnedObject *>(obj));

    if (!backp)
    {
        // insert as the only item
        QElem *e = new QElem;
        e->obj = obj;
        e->next = e->prev = NULL;
        frontp = backp = e;
        n = 1;
    }
    else if (compare==NULL)
    {
        insafter_qelem(backp, obj);
    }
    else
    {
        // priority queue: seek insertion place
        QElem *p = backp;
        while (p && compare(obj, p->obj) < 0)
            p = p->prev;
        if (p)
            insafter_qelem(p, obj);
        else
            insbefore_qelem(frontp, obj);
    }
}

void cQueueBase::insertBefore(cObject *where, cObject *obj)
{
    if (!obj)
        throw cRuntimeError(this, "cannot insert NULL pointer");

    QElem *p = find_qelem(where);
    if (!p)
        throw cRuntimeError(this, "insertBefore(w,o): object w=`%s' not in the queue", where->getName());

    if (obj->isOwnedObject() && getTakeOwnership())
        take(static_cast<cOwnedObject *>(obj));
    insbefore_qelem(p,obj);
}

void cQueueBase::insertAfter(cObject *where, cObject *obj)
{
    if (!obj)
        throw cRuntimeError(this,"cannot insert NULL pointer");

    QElem *p = find_qelem(where);
    if (!p)
        throw cRuntimeError(this, "insertAfter(w,o): object w=`%s' not in the queue",where->getName());

    if (obj->isOwnedObject() && getTakeOwnership())
        take(static_cast<cOwnedObject *>(obj));
    insafter_qelem(p,obj);
}

cObject *cQueueBase::front() const
{
    return frontp ? frontp->obj : NULL;
}

cObject *cQueueBase::back() const
{
    return backp ? backp->obj : NULL;
}

cObject *cQueueBase::remove(cObject *obj)
{
    if (!obj)
        return NULL;
    //FIXME: handle special cases faster: if obj==front() or ==back(), don't invoke find_qelem()
    QElem *p = find_qelem(obj);
    if (!p)
        return NULL;
    return remove_qelem(p);
}

cObject *cQueueBase::pop()
{
    if (!frontp)
        throw cRuntimeError(this,"pop(): queue empty");

    return remove_qelem(frontp);
}

int cQueueBase::getLength() const
{
    return n;
}

bool cQueueBase::contains(cObject *obj) const
{
    return find_qelem(obj)!=NULL;
}

cObject *cQueueBase::get(int i) const
{
    QElem *p = frontp;
    while (p!=NULL && i>0)
        p = p->next, i--;
    return p ? p->obj : NULL;
}

void cQueueBase::addLength(cObject *obj)
{
    cPacket *pkt = dynamic_cast<cPacket*>(obj);
    if (pkt)
    {
        bitlength += pkt->getBitLength();
    }
}

bool cQueueBase::insertPiggyback(cMessage *msg, int updateType)
{
    bool result = false;
    MyUDPPacket *pkt = NULL;

    if(isEmpty())
    {
        EV << "There is no packet in the queue, should generate update" << endl;
    }
    else
    {
        QElem *p = frontp;

        while(p)
        {
            pkt = dynamic_cast<MyUDPPacket*>(p->obj); //Packet already enqueued
            if(pkt)
            {
                if(pkt->getKind() == LINK_DELAY_UPDATE || pkt->getKind() == PIGGY_BACK)
                {
                    p = p->next;
                }
                else if(pkt->getKind() == TRAFFIC)
                {
                    MyUDPPacket *tempPkt = dynamic_cast<MyUDPPacket*>(msg); //The update packet
                    if(tempPkt)
                    {
                        //EV << "Test in the cqueueBase updateType is " << updateType << endl;

                        if(updateType == 1) //Link State
                        {
                            EV << "In link state, updateType is " << updateType << endl;
                            pkt->setNeighborDelayInfo(tempPkt->getNeighborDelayInfo());
                            std::map<int,double>::iterator tempIt;
                            for(tempIt = tempPkt->getNeighborDelayInfo().begin(); tempIt != tempPkt->getNeighborDelayInfo().end();\
                            tempIt++)
                            {
                                EV << "In queue test: Node " << tempIt->first << ", delay: " << tempIt->second << " " << endl;
                            }

                            EV << "Before piggyback, the packet size is: " << pkt->getByteLength() << endl;

                            int newSize = pkt->getByteLength() + 12 * pkt->getNeighborDelayInfo().size();
                            pkt->setByteLength(newSize);
                            bitlength += 12 * pkt->getNeighborDelayInfo().size() * 8;
                        }
                        else if(updateType == 2) //Distance Vector
                        {
                            EV << "In distance vector, updateType is " << updateType << endl;
                            pkt->setNeighborDelayTable(tempPkt->getNeighborDelayTable());
                        }

                        pkt->getPbUpdate().generationNode = tempPkt->getPbUpdate().generationNode;
                        pkt->getPbUpdate().sequence = tempPkt->getPbUpdate().sequence;
                        pkt->getPbUpdate().originalKind = pkt->getKind();
                        pkt->getPbUpdate().name = tempPkt->getPbUpdate().name;
                        pkt->setKind(PIGGY_BACK);
                        pkt->setTransferNode(tempPkt->getTransferNode());
                        pkt->setExpectedNeighbor(tempPkt->getExpectedNeighbor());

                        EV << "Piggyback insertion complete, PB at packet " << "(" << pkt->getFlowId() << "," << \
                                pkt->getSeqNo() << ")" << endl;
                        EV << "After piggyback, the packet size is: " << pkt->getByteLength() << endl;

                        result = true;
                        break;
                    }
                }
            }
        }

        if(p == NULL)
        {
            EV << "No packets available for piggyback, need to generate one" << endl;
        }
    }

    return result;
}

NAMESPACE_END

