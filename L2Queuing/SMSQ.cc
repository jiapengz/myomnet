//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <SMSQ.h>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include "globals.h"
#include "cexception.h"

#ifdef WITH_PARSIM
#include "ccommbuffer.h"
#endif

NAMESPACE_BEGIN

using std::ostream;


Register_Class(SMSQ);

/*SMSQ::SMSQ() {
    // TODO Auto-generated constructor stub
    bitlength = 0;
    setup(NULL);
}

SMSQ::~SMSQ() {
    // TODO Auto-generated destructor stub
}*/

SMSQ::SMSQ(const char *name, CompareFunc cmp) : cQueueBase(name,cmp)
{
    bitlength = 0;
    linkrate = 0;
    linkdelay = 0;
    type = 1;
    useSlack = 0;
}

SMSQ::SMSQ(const SMSQ& queue) : cQueueBase(queue)
{
    copy(queue);
}

void SMSQ::copy(const SMSQ& queue)
{
    bitlength = queue.bitlength;
}

SMSQ& SMSQ::operator=(const SMSQ& queue)
{
    if (this==&queue) return *this;
    cQueueBase::operator=(queue);
    copy(queue);
    return *this;
}

std::string SMSQ::info() const
{
    if (empty())
        return std::string("empty");
    std::stringstream out;
    out << "len=" << getLength() << ", " << getBitLength() << " bits (" << getByteLength() << " bytes)";
    return out.str();
}

void SMSQ::parsimPack(cCommBuffer *buffer)
{
#ifndef WITH_PARSIM
    throw cRuntimeError(this,eNOPARSIM);
#else
    cQueueBase::parsimPack(buffer);
    buffer->pack(bitlength);
#endif
}

void SMSQ::parsimUnpack(cCommBuffer *buffer)
{
#ifndef WITH_PARSIM
    throw cRuntimeError(this,eNOPARSIM);
#else
    cQueueBase::parsimUnpack(buffer);
    buffer->unpack(bitlength);
#endif
}

void SMSQ::addLength(cPacket *pkt)
{
    bitlength += pkt->getBitLength();
}

cPacket *SMSQ::checkPacket(cObject *obj)
{
    cPacket *pkt = dynamic_cast<cPacket *>(obj);
    if (!pkt)
        throw cRuntimeError(this, "insert...(): Cannot cast (%s)%s to cPacket", obj->getClassName(), obj->getFullName());
    return pkt;
}

void SMSQ::insert(cPacket *pkt) //Need re-implementation
{
    cQueueBase::insert(pkt);
    addLength(pkt);
}

int SMSQ::findFirstPlace(cObject *newPacket, cObject *oldPacket)
{
    MyUDPPacket *newpkt_t = dynamic_cast<MyUDPPacket*>(newPacket);
    MyUDPPacket *oldpkt_t = dynamic_cast<MyUDPPacket*>(oldPacket);

    if(!newpkt_t)
        return 1;
    if(!oldpkt_t)
        return 1;

    if(newpkt_t->getFlowId() == oldpkt_t->getFlowId())
        return 1;

    double dt_new, dt_old;
    dt_new = newpkt_t->getPktRequirement() - (simTime().dbl() - newpkt_t->getSendTime()) - newpkt_t->getDownStreamDelay()\
            - newpkt_t->getUpStreamDelay();
    dt_old = oldpkt_t->getPktRequirement() - (simTime().dbl() - oldpkt_t->getSendTime()) - oldpkt_t->getDownStreamDelay()\
            - oldpkt_t->getUpStreamDelay();

    if (dt_new - dt_old < 0)
        return -1;
    else
        return 1;
}

void SMSQ::insertBefore(cPacket *where, cPacket *pkt)
{
    cQueueBase::insertBefore(where, pkt);
    addLength(pkt);
}

void SMSQ::insertAfter(cPacket *where, cPacket *pkt)
{
    cQueueBase::insertAfter(where, pkt);
    addLength(pkt);
}

cPacket *SMSQ::remove(cPacket *pkt)
{
    //return (cPacket *)remove((cObject *)pkt);
    return NULL;
}

cPacket *SMSQ::pop()
{
    cPacket *pkt = (cPacket *)cQueueBase::pop();
    if (pkt)
        bitlength -= pkt->getBitLength();
    return pkt;
}

void SMSQ::clear()
{
    cQueueBase::clear();
    bitlength = 0;
}

/*void SMSQ::insert(cObject *obj)
{
    insert(checkPacket(obj));
}

void SMSQ::insertBefore(cObject *where, cObject *obj)
{
    insertBefore(checkPacket(where), checkPacket(obj));
}

void SMSQ::insertAfter(cObject *where, cObject *obj)
{
    insertAfter(checkPacket(where), checkPacket(obj));
}

cObject *SMSQ::remove(cObject *obj)
{
    cPacket *pkt = (cPacket *)cQueueBase::remove(obj);
    if (pkt)
        bitlength -= pkt->getBitLength();
    return pkt;
}*/

NAMESPACE_END
