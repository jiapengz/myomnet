//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SMSQ_H_
#define SMSQ_H_

#include "cQueueBase.h"
#include "cmessage.h"
#include "MyUDPPacket_m.h"

NAMESPACE_BEGIN

class SIM_API SMSQ : public cQueueBase
{
    private:
        int64 bitlength;
        double linkrate;
        double linkdelay;
        int type;
        bool useSlack;

    /*public:
        SMSQ();
        virtual ~SMSQ();*/

    /*private:
        void copy(const SMSQ& other);
        virtual void insert(cObject *obj);
        virtual void insertBefore(cObject *where, cObject *obj);
        virtual void insertAfter(cObject *where, cObject *obj);
        virtual cObject *remove(cObject *obj);*/

    protected:
        // internal
        void addLength(cPacket *pkt);
        cPacket *checkPacket(cObject *obj);
        void copy(const SMSQ& other);

    public:
        /** @name Constructors, destructor, assignment. */
        //@{
        /**
         * Constructor.
         */
        SMSQ(const char *name=NULL, CompareFunc cmp=NULL);

        /**
         * Copy constructor. Contained objects that are owned by the queue
         * will be duplicated so that the new queue will have its own copy
         * of them.
         */
        SMSQ(const SMSQ& queue);

        /**
         * Assignment operator. Duplication and assignment work all right with cPacketQueue.
         * Contained objects that are owned by the queue will be duplicated
         * so that the new queue will have its own copy of them.
         *
         * The name member is not copied; see cNamedObject's operator=() for more details.
         */
        SMSQ& operator=(const SMSQ& queue);
        //@}

        /** @name Redefined cObject member functions. */
        //@{

        /**
         * Duplication and assignment work all right with cPacketQueue.
         * Contained objects that are owned by the queue will be duplicated
         * so that the new queue will have its own copy of them.
         */
        virtual SMSQ *dup() const  {return new SMSQ(*this);}

        /**
         * Produces a one-line description of the object's contents.
         * See cObject for more details.
         */
        virtual std::string info() const;

        /**
         * Serializes the object into an MPI send buffer.
         * Used by the simulation kernel for parallel execution.
         * See cObject for more details.
         */
        virtual void parsimPack(cCommBuffer *buffer);

        /**
         * Deserializes the object from an MPI receive buffer
         * Used by the simulation kernel for parallel execution.
         * See cObject for more details.
         */
        virtual void parsimUnpack(cCommBuffer *buffer);
        //@}

        /** @name Setup, insertion and removal functions. */
        //@{
        /**
         * Adds an element to the back of the queue. Trying to insert a
         * NULL pointer is an error (throws cRuntimeError). The given
         * object must be an instance of cPacket.
         */
        virtual void insert(cPacket *pkt);

        /**
         * Inserts exactly before the given object. If the given position
         * does not exist or if you try to insert a NULL pointer,
         * cRuntimeError is thrown. The given object must be an instance
         * of cPacket.
         */
        virtual void insertBefore(cPacket *where, cPacket *pkt);

        /**
         * Inserts exactly after the given object. If the given position
         * does not exist or if you try to insert a NULL pointer,
         * cRuntimeError is thrown. The given object must be an instance
         * of cPacket.
         */
        virtual void insertAfter(cPacket *where, cPacket *pkt);

        /**
         * Unlinks and returns the object given. If the object is not in the
         * queue, NULL pointer is returned. The given object must be an instance
         * of cPacket.
         */
        virtual cPacket *remove(cPacket *pkt);

        /**
         * Unlinks and returns the front element in the queue. If the queue
         * is empty, cRuntimeError is thrown.
         */
        virtual cPacket *pop();

        /**
         * Empties the container. Contained objects that were owned by the
         * queue (see getTakeOwnership()) will be deleted.
         */
        virtual void clear();
        //@}

        /** @name Query functions. */
        //@{
        /**
         * Returns the total size of the messages in the queue, in bits.
         * This is the sum of the message bit lengths; see cPacket::getBitLength().
         */
        int64 getBitLength() const  {return bitlength;}

        /**
         * Returns the sum of the message lengths in bytes, that is, getBitLength()/8.
         * If getBitLength() is not a multiple of 8, the result is rounded up.
         */
        int64 getByteLength() const  {return (bitlength+7)>>3;}

        /**
         * Returns pointer to the object at the front of the queue.
         * This is the element to be return by pop().
         * Returns NULL if the queue is empty.
         */
        virtual cPacket *front() const  {return (cPacket *)cQueueBase::front();}

        /**
         * Returns pointer to the last (back) element in the queue.
         * This is the element most recently added by insert().
         * Returns NULL if the queue is empty.
         */
        virtual cPacket *back() const  {return (cPacket *)cQueueBase::back();}

        /**
         * Returns the ith element in the queue, or NULL if i is out of range.
         * get(0) returns the front element. This method performs linear
         * search.
         */
        cPacket *get(int i) const  {return (cPacket *)cQueueBase::get(i);}

        void setLinkRate(double rate) {linkrate = rate;}
        void setLinkDelay(double delay) {linkdelay = delay;}
        void setType(int qType) {type = qType;}
        void setSlakc(bool use_slack) {useSlack = use_slack;}

        int findFirstPlace(cObject *newPacket, cObject *oldPacket);
};

NAMESPACE_END

#endif /* SMSQ_H_ */
