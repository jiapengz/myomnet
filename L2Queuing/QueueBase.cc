//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <QueueBase.h>

QueueBase::QueueBase() {
    // TODO Auto-generated constructor stub
    linkdelay = 0;
    linkrate = 0;
    mean_delay = 0;
    total_delay = 0;
    total_pkt_no = 0;
}

QueueBase::~QueueBase() {
    // TODO Auto-generated destructor stub
}

void QueueBase::delayCompute()
{
    EV << "QueueBase delayCompute called" << endl;
    if(total_pkt_no == 0)
    {
        mean_delay = 0;
    }
    else
    {
        mean_delay = total_delay / total_pkt_no;
    }

    EV << "At time " << simTime().dbl() <<  ", total packet delay is: " << total_delay << ", total pkt no:" << total_pkt_no \
                    << ", mean is:" << mean_delay << endl;

    total_pkt_no = 0;
    total_delay = 0;
}

bool QueueBase::doPiggyback(cMessage *msg, int updateType)
{
    EV << "Thie is from the QueueBase class, we should implement the function in sub-classes" << endl;
    return false;
}

double QueueBase::remainingSrvTime()
{
    EV << "This is from the QueueBase class, we should implementthe function in sub-classes" << endl;
    return -1;
}
