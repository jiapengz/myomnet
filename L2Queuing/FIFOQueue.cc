//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 1992-2008 Andras Varga
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
#include "MyDelayExPkt_m.h"
#include "QueueBase.h"
#include "SimType.h"
#include "cqueueBase.h"


/**
 * Point-to-point interface module. While one frame is transmitted,
 * additional frames get queued up; see NED file for more info.
 */
class FIFOQueue : public QueueBase
{
  private:
    long frameCapacity;
    MyUDPPacket *myPkt;
    double temp_var;

    cQueueBase queue;
    cMessage *endTransmissionEvent;

    /*simsignal_t qlenSignal;
    simsignal_t busySignal;
    simsignal_t queueingTimeSignal;
    simsignal_t dropSignal;
    simsignal_t txBytesSignal;
    simsignal_t rxBytesSignal;*/

  public:
    FIFOQueue();
    virtual ~FIFOQueue();

  protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);

    virtual void startTransmitting(cMessage *msg);

    virtual void displayStatus(bool isBusy);
    virtual bool doPiggyback(cMessage *msg, int updateType);
    virtual double remainingSrvTime();
};

Define_Module(FIFOQueue);

FIFOQueue::FIFOQueue()
{
    endTransmissionEvent = NULL;
}

FIFOQueue::~FIFOQueue()
{
    cancelAndDelete(endTransmissionEvent);
}

void FIFOQueue::initialize()
{
    queue.setName("FIFOqueue");
    endTransmissionEvent = new cMessage("endTxEvent");

    frameCapacity = par("frameCapacity");

    total_delay = 0;
    mean_delay = 0;
    total_pkt_no = 0;

    channel = dynamic_cast<cDatarateChannel*>(gate("line$o")->getTransmissionChannel());
    if (channel)
    {
        linkrate = channel->getDatarate();
        linkdelay = channel->getDelay().dbl();
    }

    EV << "FIFOQueue: linkrate: " << linkrate << ", linkdelay: " << linkdelay << endl;
    /*qlenSignal = registerSignal("qlen");
    busySignal = registerSignal("busy");
    queueingTimeSignal = registerSignal("queueingTime");
    dropSignal = registerSignal("drop");
    txBytesSignal = registerSignal("txBytes");
    rxBytesSignal = registerSignal("rxBytes");

    emit(qlenSignal, queue.length());
    emit(busySignal, false);*/
}

void FIFOQueue::startTransmitting(cMessage *msg)
{
    if (ev.isGUI()) displayStatus(true);

    EV << "Starting transmission of " << msg << endl;
    //int64 numBytes = check_and_cast<cPacket *>(msg)->getByteLength();

    myPkt = dynamic_cast<MyUDPPacket*>(msg);
    if(myPkt && myPkt->getKind() != LINK_DELAY_UPDATE)
    {
        total_pkt_no++;
        temp_var = simTime().dbl() - myPkt->getLastEnqueueTime() + myPkt->getBitLength() / linkrate;
        total_delay += temp_var;
        EV << "At time " << simTime().dbl() << ", enqueue at " << myPkt->getLastEnqueueTime() << ", delay at the link is: " \
                << temp_var << ", total delay: " << total_delay << ", total No.: " << total_pkt_no << endl << endl;
    }

    send(msg, "line$o");

    //emit(txBytesSignal, (long)numBytes);

    // Schedule an event for the time when last bit will leave the gate.
    simtime_t endTransmission = gate("line$o")->getTransmissionChannel()->getTransmissionFinishTime();
    scheduleAt(endTransmission, endTransmissionEvent);
}

void FIFOQueue::handleMessage(cMessage *msg)
{
    if (msg==endTransmissionEvent)
    {
        // Transmission finished, we can start next one.
        EV << "Transmission finished.\n";
        if (ev.isGUI()) displayStatus(false);
        if (queue.empty())
        {
            //emit(busySignal, false);
            EV << "Empty queue" << endl;
        }
        else
        {
            msg = (cMessage *) queue.pop();
            //emit(queueingTimeSignal, simTime() - msg->getTimestamp());
            //emit(qlenSignal, queue.length());
            startTransmitting(msg);
        }
    }
    else if (msg->arrivedOn("line$i"))
    {
        // pass up
        //emit(rxBytesSignal, (long)check_and_cast<cPacket *>(msg)->getByteLength());
        send(msg,"out");
    }
    else // arrived on gate "in"
    {
        myPkt = dynamic_cast<MyUDPPacket*>(msg);
        if(myPkt)
        {
            myPkt->setLastEnqueueTime(simTime().dbl());
            EV << "Current Time: " << myPkt->getLastEnqueueTime() << endl << endl;
        }

        if (endTransmissionEvent->isScheduled())
        {
            // We are currently busy, so just queue up the packet.
            if (frameCapacity && queue.length()>=frameCapacity)
            {
                EV << "Received " << msg << " but transmitter busy and queue full: discarding\n";
                //emit(dropSignal, (long)check_and_cast<cPacket *>(msg)->getByteLength());
                delete msg;
            }
            else
            {
                EV << "Received " << msg << " but transmitter busy: queueing up\n";
                msg->setTimestamp();
                queue.insert(msg);
                //emit(qlenSignal, queue.length());
            }
        }
        else
        {
            // We are idle, so we can start transmitting right away.
            EV << "Received " << msg << endl;
            //emit(queueingTimeSignal, SIMTIME_ZERO);
            startTransmitting(msg);
            //emit(busySignal, true);
        }
    }
}

void FIFOQueue::displayStatus(bool isBusy)
{
    getDisplayString().setTagArg("t",0, isBusy ? "transmitting" : "idle");
    getDisplayString().setTagArg("i",1, isBusy ? (queue.length()>=3 ? "red" : "yellow") : "");
}

bool FIFOQueue::doPiggyback(cMessage *msg, int updateType)
{
    bool result;

    EV << "Piggyback from the FIFOQueue" << endl;
    result = queue.insertPiggyback(msg,updateType);

    return result;
}

double FIFOQueue::remainingSrvTime()
{
    EV << "This is a FIFO Queue, usually we do not need to count the remaining time here" << endl;
    return -1;
}
