//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef ASISTMODULE_H_
#define ASISTMODULE_H_

#include <omnetpp.h>
#include <map>

class AssistModule : public cSimpleModule
{
    public:
        struct assistMod
        {
            double du_mean;
            double du_std;
            double dd_mean;
            double dd_std;
            unsigned int number;
        };

        typedef std::map<int,assistMod> assistMap;

        assistMap dataBase;

    public:
        AssistModule();
        virtual ~AssistModule();

    protected:
        virtual void initialize();
        virtual void handleMessage(cMessage *msg);

    public:
        virtual void initialMap();
        virtual void mapScan();

    private:
        bool useModule;
        bool isTest;
        std::string flowId;
};

#endif /* ASISTMODULE_H_ */
