//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <AssistModule/AssistModule.h>

Define_Module(AssistModule);

cNEDValue printNo(cComponent *context, cNEDValue argv[], int argc)
{
    EV << argv[0].doubleValue() << " " << argv[1].doubleValue() << endl;
    return argv[0];
}

Define_NED_Function(printNo, "double printNo(...)")

AssistModule::AssistModule() {
    // TODO Auto-generated constructor stub

}

AssistModule::~AssistModule() {
    // TODO Auto-generated destructor stub
}

void AssistModule::initialize()
{
    /*EV << "Start Initialization" << endl;
    const char *ptr = par("flowId").stringValue();
    flowId = par("flowId").stdstringValue();
    if (flowId.empty())
        EV << "You enter an empty string." << endl;
    else
    {
        EV << flowId << endl;
        if (!flowId.compare("NULL1"))
            EV << "The content of string is \"NULL\"" << endl;
    }

    std::string str = "This is a test";
    EV << str << endl;*/


    useModule = par("useModule").boolValue();
    isTest = par("isAMTest").boolValue();
    if (!useModule)
    {
        EV << "We don'use this module" << endl;

    }
    else
    {
        initialMap();
        if (isTest)
            mapScan();
    }
}

void AssistModule::handleMessage(cMessage *msg)
{
    EV << "Receive a message" << endl;
}

void AssistModule::initialMap()
{
    const char *ptr;

    const char *flowToken;
    const char *duMeanToken;
    const char *duStdToken;
    const char *dDMeanToken;
    const char *dDStdToken;

    ptr = par("flowId");
    cStringTokenizer flowIdTokenizer(ptr);

    ptr = par("duMean");
    cStringTokenizer duMeanTokenizer(ptr);

    ptr = par("duStd");
    cStringTokenizer duStdTokenizer(ptr);

    ptr = par("ddMean");
    cStringTokenizer dDMeanTokenizer(ptr);

    ptr = par("ddStd");
    cStringTokenizer dDStdTokenizer(ptr);

    int i;

    while ((flowToken = flowIdTokenizer.nextToken())!=NULL)
    {
        i = atoi(flowToken);
        assistMod *aMpt = new assistMod();

        if ((duMeanToken = duMeanTokenizer.nextToken())!=NULL)
                aMpt->du_mean = atof(duMeanToken);
        else
            aMpt->du_mean = 0.1;

        if ((duStdToken = duStdTokenizer.nextToken())!=NULL)
            aMpt->du_std = atof(duStdToken);
        else
            aMpt->du_std = 0.2;

        if ((dDMeanToken = dDMeanTokenizer.nextToken())!=NULL)
            aMpt->dd_mean = atof(dDMeanToken);
        else
            aMpt->dd_mean = 0.3;

        if ((dDStdToken = dDStdTokenizer.nextToken())!=NULL)
            aMpt->dd_std = atof(dDStdToken);
        else
            aMpt->dd_std = 0.4;

        aMpt->number = 0;

        dataBase.insert(std::pair<int,assistMod>(i, *aMpt));
        //EV << "Insert completes" << endl;
    }
}

void AssistModule::mapScan()
{
    std::map<int,assistMod>::iterator it;

    for (it = dataBase.begin(); it != dataBase.end(); it++)
    {
        EV << "FlowId: " << it->first << ", du_mean: " << it->second.du_mean << ", du_std: " \
                << it->second.du_std << ", dd_mean: " << it->second.dd_mean << ", dd_std: " << it->second.dd_std <<endl;
    }
}
