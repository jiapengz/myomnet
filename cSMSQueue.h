//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef CSMSQUEUE_H_
#define CSMSQUEUE_H_

#include "cqueueBase.h"
#include "AssistModule.h"
#include "SimType.h"

class cSMSQueue : public cQueueBase
{
    public:
        cSMSQueue(const char *name = NULL, MyCompareFunc cmp = NULL);
        virtual ~cSMSQueue();

    private:
        bool useSlack;
        int queueType;
        bool pmuFirst;
        unsigned int enqueue_n;
        unsigned int dequeue_n;
        int pktBefore;
        int64 qLengthBefore;
        simtime_t srvStartTime;
        AssistModule *amptr;

    private:
        void copy(const cSMSQueue& other);

    public:
        cSMSQueue(const cSMSQueue& queue);

        cSMSQueue& operator=(const cSMSQueue& queue);

        virtual cSMSQueue *dup() const  {return new cSMSQueue(*this);}

        virtual std::string info() const;


        /**
         * Returns the total size of the messages in the queue, in bits.
         * This is the sum of the message bit lengths; see cPacket::getBitLength().
         */
        int64 getBitLength() const  {return bitlength;}

        /**
         * Returns the sum of the message lengths in bytes, that is, getBitLength()/8.
         * If getBitLength() is not a multiple of 8, the result is rounded up.
         */
        int64 getByteLength() const  {return (bitlength+7)>>3;}

        /**
        * Adds an element to the back of the queue. Trying to insert a
        * NULL pointer is an error (throws cRuntimeError). The queue uses
        * SMS type.
        */
        virtual void insert(cObject *obj);


        /**
        * Unlinks and returns the front element in the queue. If the queue
        * was empty, cRuntimeError is thrown. Modified a little.
        */
        virtual cObject *pop();

        void setUseSlack(bool use_slack) {useSlack = use_slack; EV << "useSlack: " << useSlack << endl;}
        void setQueueType(int type) {queueType = type;}
        void setTrafficPriority(bool pmu_first) {pmuFirst = pmu_first;}
        void setAssistModPtr(AssistModule *AM) {amptr = AM; EV << "AM pointer set" << endl;}
        AssistModule *getAssisModPtr() {return amptr;}
        void init() {bitlength = enqueue_n = dequeue_n = qLengthBefore = 0; linkrate = 1000000;}
        void setSrvStartTime(simtime_t time) {srvStartTime = time;}

        int findFirstPlace(cObject *newPacket, cObject *oldPacket);

        void testFunc1(cObject *obj);
        void testFunc2();
        void testFunc3();

};

#endif /* CSMSQUEUE_H_ */
