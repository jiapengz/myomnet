//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "CBRTraffic.h"

Define_Module(CBRTraffic);

CBRTraffic::CBRTraffic() {
    // TODO Auto-generated constructor stub
    selfMsg = NULL;
    numSent = 0;
}

CBRTraffic::~CBRTraffic() {
    // TODO Auto-generated destructor stub
    cancelAndDelete(selfMsg);
}

void CBRTraffic::initialize(int stage)
{
    MyAppBase::initialize(stage);

    if (stage == 0)
    {
        startTime = par("startTime").doubleValue();
        stopTime = par("stopTime").doubleValue();
        packetLength = par("packetLength");
        sendrate = par("sendrate").doubleValue();
        requirement = par("flowRequirement").doubleValue();
        flowId = par("flowId");
        myAddr = par("address");
        destinationAddr = par("destAddr");
        randomStart = par("randomStart").boolValue();
        firstHopRate = par("firstHopRate").doubleValue();
        extraDelayRange = par("extraDelayRange").doubleValue();
        binNo = par("binNumber");

        extraStartDelay = 0;
        rdStartIndex = 0;

        manualDU = par("manualUpDelay").doubleValue();
        manualDD = par("manualDownDelay").doubleValue();

        if(randomStart)
        {
            rdStartIndex = intuniform(0, binNo - 1);
            extraStartDelay = rdStartIndex * extraDelayRange.dbl() / binNo;
        }
        EV << "$$$$$$   Initialing: The random start index is " << rdStartIndex << ", adjustment to requirement is " << \
                extraStartDelay << ". The flow number is " << flowId << "   $$$$$$" << endl;

        if (stopTime >= SIMTIME_ZERO && stopTime < startTime)
            error("Invalid startTime/stopTime parameters");
        selfMsg = new cMessage("sendTimer");
        //std::cout << "CBRTraffic initialize" << endl;
        //std::cout << "Length " << packetLength << " rate " << sendrate << endl;
        //EV << "Address: " << myAddr << " Parent Id: " << getParentModule()->getId() << " Parent index: " << getParentModule()->getIndex() << endl;
        //EV << "Parent Name: " << getParentModule()->getName() << " Parent Info: " << getParentModule()->info() << endl;
        //EV << "My Ned TypeName: " << getNedTypeName() << " Parent Ned TypeName: " << getParentModule()->getNedTypeName() << endl;
    }

}

void CBRTraffic::finish()
{
    MyAppBase::finish();
}

void CBRTraffic::handleMessageWhenUp(cMessage *msg)
{
    if (msg->isSelfMessage())
    {
        ASSERT(msg == selfMsg);
        switch (selfMsg->getKind()) {
            case START: processStart(); break;
            case SEND:  processSend(); break;
            case STOP:  processStop(); break;
            default: throw cRuntimeError("Invalid kind %d in self message", (int)selfMsg->getKind());
        }
    }
    else
    {
        error("Unrecognized message (%s)%s", msg->getClassName(), msg->getName());
    }

    if (ev.isGUI())
    {
        //char buf[40];
        //sprintf(buf, "rcvd: %d pks\nsent: %d pks", numReceived, numSent);
        //getDisplayString().setTagArg("t", 0, buf);
        EV << "Receive a message" << endl;
    }
}

void CBRTraffic::processStart()
{
    if (true)
    {
        selfMsg->setKind(SEND);
        processSend();
    }
    else
    {
        if (stopTime >= SIMTIME_ZERO)
        {
            selfMsg->setKind(STOP);
            scheduleAt(stopTime, selfMsg);
        }
    }
}

void CBRTraffic::processSend()
{
    sendPacket();
    simtime_t d = simTime() + (packetLength * 8)/sendrate;

    if (stopTime < SIMTIME_ZERO || d < stopTime)
    {
        selfMsg->setKind(SEND);
        scheduleAt(d, selfMsg);
    }
    else
    {
        selfMsg->setKind(STOP);
        scheduleAt(stopTime, selfMsg);
    }
}

void CBRTraffic::processStop()
{
    EV << "Application stops" << endl;
}

void CBRTraffic::sendPacket()
{
    EV << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << endl;
    EV << "Sending a packet at " << simTime() <<endl;
    numSent++;
    char msgName[32];
    sprintf(msgName, "UDP-CBR-%d-Flow-%d-AppData-%d", myAddr, flowId, numSent);
    MyUDPPacket *payload = new MyUDPPacket(msgName);
    payload->setByteLength(packetLength);
    payload->setDestinationPort(1);
    payload->setSourcePort(2);
    payload->setSourceAddr(myAddr);
    payload->setDestinationAddr(destinationAddr);
    payload->setKind(TRAFFIC);
    payload->setSeqNo(numSent);
    payload->setFlowId(flowId);
    payload->setSendTime(simTime().dbl());
    payload->setLastEnqueueTime(0);
    payload->setPktRequirement(requirement);

    if(randomStart)
    {
        //payload->setPktRequirement(requirement - extraStartDelay.dbl());
        EV << "With random start, index of " << rdStartIndex << " the remaining time when the packet is sent out: " << \
                requirement - extraStartDelay.dbl() << endl;
    }

    payload->setUpStreamDelay(manualDU);
    payload->setDownStreamDelay(manualDD);

    /*if(flowId == 0)
    {
        payload->setUpStreamDelay(0.031);
        payload->setDownStreamDelay(0.015);
    }
    else
    {
        payload->setUpStreamDelay(0.022);
        payload->setDownStreamDelay(0.005);
    }*/

    EV << "Length of a packet " << payload->getBitLength() <<endl;
    //EV << "GetIndex " << getIndex() << " GetID " << getId() << endl;

    sendDelayed(payload, extraStartDelay, "appOut");
}

bool CBRTraffic::startApp()
{
    simtime_t start = std::max(startTime, simTime());

    if (stopTime < SIMTIME_ZERO || startTime < SIMTIME_ZERO)
    {
        EV << "Node " << myAddr << ", application not used" << endl;
    }
    else if ((start < stopTime) || (start == stopTime && startTime == stopTime))
    {
        selfMsg->setKind(START);
        scheduleAt(start + (packetLength * 8 / sendrate), selfMsg);
        /*if(randomStart == false)
        {
            scheduleAt(start + (packetLength * 8 / sendrate), selfMsg);
        }
        else
        {
            simtime_t newStart = start + (packetLength * 8 / sendrate) + rdStartIndex * packetLength * 8 / firstHopRate;
            EV << "Node " << myAddr << " starts at " << newStart << ", the delay is " << \
                    rdStartIndex * packetLength * 8 / firstHopRate << endl;
            scheduleAt(newStart, selfMsg);
        }*/
    }
    return true;
}

bool CBRTraffic::stopApp()
{
    if (selfMsg)
        cancelEvent(selfMsg);
    return true;
}

bool CBRTraffic::crashApp()
{
    if (selfMsg)
        cancelEvent(selfMsg);
    return true;
}
