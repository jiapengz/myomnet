//
// Generated file, do not edit! Created by opp_msgc 4.4 from UDPApp/MyPDCPacket.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#include <iostream>
#include <sstream>
#include "MyPDCPacket_m.h"

USING_NAMESPACE

// Template rule which fires if a struct or class doesn't have operator<<
template<typename T>
std::ostream& operator<<(std::ostream& out,const T&) {return out;}

// Another default rule (prevents compiler from choosing base class' doPacking())
template<typename T>
void doPacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doPacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}

template<typename T>
void doUnpacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doUnpacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}




Register_Class(MyPDCPacket);

MyPDCPacket::MyPDCPacket(const char *name, int kind) : ::MyUDPPacket(name,kind)
{
}

MyPDCPacket::MyPDCPacket(const MyPDCPacket& other) : ::MyUDPPacket(other)
{
    copy(other);
}

MyPDCPacket::~MyPDCPacket()
{
}

MyPDCPacket& MyPDCPacket::operator=(const MyPDCPacket& other)
{
    if (this==&other) return *this;
    ::MyUDPPacket::operator=(other);
    copy(other);
    return *this;
}

void MyPDCPacket::copy(const MyPDCPacket& other)
{
    this->pktPtrVtr_var = other.pktPtrVtr_var;
}

void MyPDCPacket::parsimPack(cCommBuffer *b)
{
    ::MyUDPPacket::parsimPack(b);
    doPacking(b,this->pktPtrVtr_var);
}

void MyPDCPacket::parsimUnpack(cCommBuffer *b)
{
    ::MyUDPPacket::parsimUnpack(b);
    doUnpacking(b,this->pktPtrVtr_var);
}

MyUDPPktPtrVtr& MyPDCPacket::getPktPtrVtr()
{
    return pktPtrVtr_var;
}

void MyPDCPacket::setPktPtrVtr(const MyUDPPktPtrVtr& pktPtrVtr)
{
    this->pktPtrVtr_var = pktPtrVtr;
}

class MyPDCPacketDescriptor : public cClassDescriptor
{
  public:
    MyPDCPacketDescriptor();
    virtual ~MyPDCPacketDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(MyPDCPacketDescriptor);

MyPDCPacketDescriptor::MyPDCPacketDescriptor() : cClassDescriptor("MyPDCPacket", "MyUDPPacket")
{
}

MyPDCPacketDescriptor::~MyPDCPacketDescriptor()
{
}

bool MyPDCPacketDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<MyPDCPacket *>(obj)!=NULL;
}

const char *MyPDCPacketDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int MyPDCPacketDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 1+basedesc->getFieldCount(object) : 1;
}

unsigned int MyPDCPacketDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISCOMPOUND,
    };
    return (field>=0 && field<1) ? fieldTypeFlags[field] : 0;
}

const char *MyPDCPacketDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "pktPtrVtr",
    };
    return (field>=0 && field<1) ? fieldNames[field] : NULL;
}

int MyPDCPacketDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='p' && strcmp(fieldName, "pktPtrVtr")==0) return base+0;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *MyPDCPacketDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "MyUDPPktPtrVtr",
    };
    return (field>=0 && field<1) ? fieldTypeStrings[field] : NULL;
}

const char *MyPDCPacketDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int MyPDCPacketDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    MyPDCPacket *pp = (MyPDCPacket *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string MyPDCPacketDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    MyPDCPacket *pp = (MyPDCPacket *)object; (void)pp;
    switch (field) {
        case 0: {std::stringstream out; out << pp->getPktPtrVtr(); return out.str();}
        default: return "";
    }
}

bool MyPDCPacketDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    MyPDCPacket *pp = (MyPDCPacket *)object; (void)pp;
    switch (field) {
        default: return false;
    }
}

const char *MyPDCPacketDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        "MyUDPPktPtrVtr",
    };
    return (field>=0 && field<1) ? fieldStructNames[field] : NULL;
}

void *MyPDCPacketDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    MyPDCPacket *pp = (MyPDCPacket *)object; (void)pp;
    switch (field) {
        case 0: return (void *)(&pp->getPktPtrVtr()); break;
        default: return NULL;
    }
}


