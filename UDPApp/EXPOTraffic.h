//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef EXPOTRAFFIC_H_
#define EXPOTRAFFIC_H_

#include <omnetpp.h>
#include "MyAppBase.h"

class EXPOTraffic : public MyAppBase
{
    protected:
        enum SelfMsgKinds { START = 1, SEND, STOP };

        int packetLength;
        double sendrate;
        double firstHopRate; // the rate out of the first hop
        simtime_t startTime;
        simtime_t stopTime;
        cMessage *selfMsg;
        unsigned int numSent;
        int flowId;

        double manualDU;
        double manualDD;
        double requirement;

        simtime_t burstTime;
        simtime_t idleTime;
        simtime_t nextIdleTime;
        simtime_t interval_; //the interval for generating a packet

    public:
        EXPOTraffic();
        virtual ~EXPOTraffic();

    protected:
        virtual int numInitStages() const {return 3;}
        virtual void initialize(int stage);
        virtual void handleMessageWhenUp(cMessage *msg);
        virtual void finish();
        virtual void sendPacket();

        virtual void processStart();
        virtual void processSend();
        virtual void processStop();

        //AppBase:
        bool startApp();
        bool stopApp();
        bool crashApp();
};

#endif /* EXPOTRAFFIC_H_ */
