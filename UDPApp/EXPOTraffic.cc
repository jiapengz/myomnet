//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "EXPOTraffic.h"

Define_Module(EXPOTraffic);

EXPOTraffic::EXPOTraffic() {
    // TODO Auto-generated constructor stub
    selfMsg = NULL;
    numSent = 0;

}

EXPOTraffic::~EXPOTraffic() {
    // TODO Auto-generated destructor stub
    cancelAndDelete(selfMsg);
}

void EXPOTraffic::initialize(int stage)
{
    MyAppBase::initialize(stage);

    if (stage == 0)
    {
        startTime = par("startTime").doubleValue();
        stopTime = par("stopTime").doubleValue();
        packetLength = par("packetLength");
        sendrate = par("sendrate").doubleValue();
        interval_ = (packetLength * 8)/sendrate;
        requirement = par("flowRequirement").doubleValue();
        flowId = par("flowId");
        myAddr = par("address");
        destinationAddr = par("destAddr");

        randomStart = par("randomStart").boolValue();
        firstHopRate = par("firstHopRate").doubleValue();
        extraDelayRange = par("extraDelayRange").doubleValue();
        binNo = par("binNumber");

        extraStartDelay = 0;
        rdStartIndex = 0;

        manualDU = par("manualUpDelay").doubleValue();
        manualDD = par("manualDownDelay").doubleValue();

        burstTime = par("expoBurstTime").doubleValue();
        idleTime = par("expoIdleTime").doubleValue();
        nextIdleTime = startTime + std::max(interval_, exponential(burstTime));
        if (stopTime >= SIMTIME_ZERO && stopTime < startTime)
            error("Invalid startTime/stopTime parameters");

        selfMsg = new cMessage("sendTimer");
        EV << "$$$$$$   Initialing: Address: " << myAddr << ", burstTime: " << burstTime << ", idleTime: " <<\
                idleTime << "   $$$$$$" << endl;
//        std::cout << "EXPOTraffic initialize" << endl;
//        std::cout << "Length " << packetLength << " rate " << sendrate << endl;
//        EV << "Address: " << myAddr << " Parent Id: " << getParentModule()->getId() << "Parent index: " << getParentModule()->getIndex() << endl;
//        EV << "Parent Name: " << getParentModule()->getName() << " Parent Info: " << getParentModule()->info() << endl;
//        EV << "My Ned TypeName: " << getNedTypeName() << " Parent Ned TypeName: " << getParentModule()->getNedTypeName() << endl;
    }

}

void EXPOTraffic::finish()
{
    MyAppBase::finish();
}

void EXPOTraffic::handleMessageWhenUp(cMessage *msg)
{
    if (msg->isSelfMessage())
    {
        ASSERT(msg == selfMsg);
        switch (selfMsg->getKind()) {
            case START: processStart(); break;
            case SEND:  processSend(); break;
            case STOP:  processStop(); break;
            default: throw cRuntimeError("Invalid kind %d in self message", (int)selfMsg->getKind());
        }
    }
    else
    {
        error("Unrecognized message (%s)%s", msg->getClassName(), msg->getName());
    }

    if (ev.isGUI())
    {
        //char buf[40];
        //sprintf(buf, "rcvd: %d pks\nsent: %d pks", numReceived, numSent);
        //getDisplayString().setTagArg("t", 0, buf);
        //EV << "Receive a message" << endl;
        ;
    }
}

void EXPOTraffic::processStart()
{
    if (true)
    {
        selfMsg->setKind(SEND);
        processSend();
    }
    else
    {
        if (stopTime >= SIMTIME_ZERO)
        {
            selfMsg->setKind(STOP);
            scheduleAt(stopTime, selfMsg);
        }
    }
}

void EXPOTraffic::processSend()
{
    sendPacket();
    simtime_t d = simTime() + interval_;

    if (d < nextIdleTime && d < stopTime)
    {
        selfMsg->setKind(SEND);
        scheduleAt(d, selfMsg);
    }
    else if (d >= nextIdleTime && d < stopTime)
    {
        EV << "In the 2nd if in processSend, " << "now: " << simTime() << " rest-start: " << nextIdleTime.dbl() <<endl;
        selfMsg->setKind(SEND);
        simtime_t nextBurstLength = std::max(interval_, exponential(burstTime)); // calculate the burst-length for the next cycle

        /*int integer1 = int(nextBurstLength / interval_ + 0.5);
        EV << "old burst-length: " << nextBurstLength.dbl();
        nextBurstLength = integer1 * interval_;
        EV << ", the rounded length is: " << nextBurstLength.dbl() << endl;*/

        d = exponential(idleTime); //calculate the idle-length for this cycle

        simtime_t tempTime = nextIdleTime;
        nextIdleTime = tempTime + d + nextBurstLength; //calculate the next time point for "idle state"
        EV << "Rest for " << d.dbl() << "s, current idle-time-point: " << tempTime.dbl() << ", next burst length: " << \
                nextBurstLength.dbl() << "s" << endl <<endl << endl;
        if (d + simTime() >= stopTime)
        {
            selfMsg->setKind(STOP);
            scheduleAt(stopTime, selfMsg);
        }
        else
            scheduleAt(tempTime+d+interval_, selfMsg); //the "out time" of the 1st packet for the next period
    }
    else
    {
        selfMsg->setKind(STOP);
        scheduleAt(stopTime, selfMsg);
    }
}

void EXPOTraffic::processStop()
{
    EV << "Application stops" << endl;
}

void EXPOTraffic::sendPacket()
{
    EV << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << endl;
    EV << "Sending a packet at " << simTime() <<endl;
    numSent++;
    char msgName[32];
    sprintf(msgName, "UDP-EXPO-%d-Flow-%d-AppData-%d", myAddr, flowId, numSent);
    MyUDPPacket *payload = new MyUDPPacket(msgName);
    payload->setByteLength(packetLength);
    payload->setDestinationPort(1);
    payload->setSourcePort(2);
    payload->setSourceAddr(myAddr);
    payload->setDestinationAddr(destinationAddr);
    payload->setKind(TRAFFIC);
    payload->setSeqNo(numSent);
    payload->setFlowId(flowId);
    payload->setSendTime(simTime().dbl());
    payload->setLastEnqueueTime(0);
    payload->setPktRequirement(requirement);

    payload->setUpStreamDelay(manualDU);
    payload->setDownStreamDelay(manualDD);

    EV << "Length of a packet " << payload->getBitLength() << ", interval: " << interval_.dbl() << endl;
    //EV << "GetIndex " << getIndex() << " GetID " << getId() << endl;

    send(payload, "appOut");
}

bool EXPOTraffic::startApp()
{
    simtime_t start = std::max(startTime, simTime());

    if (stopTime < SIMTIME_ZERO)
    {
        EV << "Node " << myAddr << ", application not used" << endl;
    }
    else if ((start < stopTime) || (start == stopTime && startTime == stopTime))
    {
        selfMsg->setKind(START);
        scheduleAt(start+interval_, selfMsg);
    }
    return true;
}

bool EXPOTraffic::stopApp()
{
    if (selfMsg)
        cancelEvent(selfMsg);
    return true;
}

bool EXPOTraffic::crashApp()
{
    if (selfMsg)
        cancelEvent(selfMsg);
    return true;
}

