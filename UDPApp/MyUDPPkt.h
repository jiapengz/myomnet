/*
 * MyUDPPkt.h
 *
 *  Created on: May 19, 2014
 *      Author: Zhang Jiapeng
 */

#ifndef MYUDPPKT_H_
#define MYUDPPKT_H_

#include <vector>
#include <string>
#include "SimType.h"

struct testDelayRpt
{
    int nid;
    double Est;
    double recTime;
};

struct piggybackInfo
{
    int generationNode;
    long sequence;
    int originalKind;
    std::string name;
};

typedef std::vector<testDelayRpt> delayRptVector;
typedef std::vector<double> dsRptVector;



#endif /* MYUDPPKT_H_ */
