//
// Generated file, do not edit! Created by opp_msgc 4.4 from UDPApp/MyUDPPacket.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#include <iostream>
#include <sstream>
#include "MyUDPPacket_m.h"

USING_NAMESPACE

// Template rule which fires if a struct or class doesn't have operator<<
template<typename T>
std::ostream& operator<<(std::ostream& out,const T&) {return out;}

// Another default rule (prevents compiler from choosing base class' doPacking())
template<typename T>
void doPacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doPacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}

template<typename T>
void doUnpacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doUnpacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}




Register_Class(MyUDPPacket);

MyUDPPacket::MyUDPPacket(const char *name, int kind) : ::cPacket(name,kind)
{
    this->sourceAddr_var = 0;
    this->destinationAddr_var = 0;
    this->sourcePort_var = 0;
    this->destinationPort_var = 0;
    this->hopCount_var = 0;
    this->sendTime_var = 0;
    this->lastEnqueueTime_var = 0;
    this->pktRequirement_var = 0;
    this->flowId_var = 0;
    this->seqNo_var = 0;
    this->upStreamDelay_var = 0;
    this->downStreamDelay_var = 0;
    this->isSource_var = 0;
    this->isCenter_var = 0;
    this->realTimeDownStreamDelay_var = 0;
    this->transferNode_var = 0;
    this->expectedNeighbor_var = 0;
}

MyUDPPacket::MyUDPPacket(const MyUDPPacket& other) : ::cPacket(other)
{
    copy(other);
}

MyUDPPacket::~MyUDPPacket()
{
}

MyUDPPacket& MyUDPPacket::operator=(const MyUDPPacket& other)
{
    if (this==&other) return *this;
    ::cPacket::operator=(other);
    copy(other);
    return *this;
}

void MyUDPPacket::copy(const MyUDPPacket& other)
{
    this->sourceAddr_var = other.sourceAddr_var;
    this->destinationAddr_var = other.destinationAddr_var;
    this->sourcePort_var = other.sourcePort_var;
    this->destinationPort_var = other.destinationPort_var;
    this->hopCount_var = other.hopCount_var;
    this->sendTime_var = other.sendTime_var;
    this->lastEnqueueTime_var = other.lastEnqueueTime_var;
    this->pktRequirement_var = other.pktRequirement_var;
    this->flowId_var = other.flowId_var;
    this->seqNo_var = other.seqNo_var;
    this->upStreamDelay_var = other.upStreamDelay_var;
    this->downStreamDelay_var = other.downStreamDelay_var;
    this->isSource_var = other.isSource_var;
    this->isCenter_var = other.isCenter_var;
    this->realTimeDownStreamDelay_var = other.realTimeDownStreamDelay_var;
    this->rptVector_var = other.rptVector_var;
    this->downStreamSeen_var = other.downStreamSeen_var;
    this->transferNode_var = other.transferNode_var;
    this->expectedNeighbor_var = other.expectedNeighbor_var;
    this->neighborDelayInfo_var = other.neighborDelayInfo_var;
    this->neighborDelayTable_var = other.neighborDelayTable_var;
    this->pbUpdate_var = other.pbUpdate_var;
}

void MyUDPPacket::parsimPack(cCommBuffer *b)
{
    ::cPacket::parsimPack(b);
    doPacking(b,this->sourceAddr_var);
    doPacking(b,this->destinationAddr_var);
    doPacking(b,this->sourcePort_var);
    doPacking(b,this->destinationPort_var);
    doPacking(b,this->hopCount_var);
    doPacking(b,this->sendTime_var);
    doPacking(b,this->lastEnqueueTime_var);
    doPacking(b,this->pktRequirement_var);
    doPacking(b,this->flowId_var);
    doPacking(b,this->seqNo_var);
    doPacking(b,this->upStreamDelay_var);
    doPacking(b,this->downStreamDelay_var);
    doPacking(b,this->isSource_var);
    doPacking(b,this->isCenter_var);
    doPacking(b,this->realTimeDownStreamDelay_var);
    doPacking(b,this->rptVector_var);
    doPacking(b,this->downStreamSeen_var);
    doPacking(b,this->transferNode_var);
    doPacking(b,this->expectedNeighbor_var);
    doPacking(b,this->neighborDelayInfo_var);
    doPacking(b,this->neighborDelayTable_var);
    doPacking(b,this->pbUpdate_var);
}

void MyUDPPacket::parsimUnpack(cCommBuffer *b)
{
    ::cPacket::parsimUnpack(b);
    doUnpacking(b,this->sourceAddr_var);
    doUnpacking(b,this->destinationAddr_var);
    doUnpacking(b,this->sourcePort_var);
    doUnpacking(b,this->destinationPort_var);
    doUnpacking(b,this->hopCount_var);
    doUnpacking(b,this->sendTime_var);
    doUnpacking(b,this->lastEnqueueTime_var);
    doUnpacking(b,this->pktRequirement_var);
    doUnpacking(b,this->flowId_var);
    doUnpacking(b,this->seqNo_var);
    doUnpacking(b,this->upStreamDelay_var);
    doUnpacking(b,this->downStreamDelay_var);
    doUnpacking(b,this->isSource_var);
    doUnpacking(b,this->isCenter_var);
    doUnpacking(b,this->realTimeDownStreamDelay_var);
    doUnpacking(b,this->rptVector_var);
    doUnpacking(b,this->downStreamSeen_var);
    doUnpacking(b,this->transferNode_var);
    doUnpacking(b,this->expectedNeighbor_var);
    doUnpacking(b,this->neighborDelayInfo_var);
    doUnpacking(b,this->neighborDelayTable_var);
    doUnpacking(b,this->pbUpdate_var);
}

int MyUDPPacket::getSourceAddr() const
{
    return sourceAddr_var;
}

void MyUDPPacket::setSourceAddr(int sourceAddr)
{
    this->sourceAddr_var = sourceAddr;
}

int MyUDPPacket::getDestinationAddr() const
{
    return destinationAddr_var;
}

void MyUDPPacket::setDestinationAddr(int destinationAddr)
{
    this->destinationAddr_var = destinationAddr;
}

int MyUDPPacket::getSourcePort() const
{
    return sourcePort_var;
}

void MyUDPPacket::setSourcePort(int sourcePort)
{
    this->sourcePort_var = sourcePort;
}

int MyUDPPacket::getDestinationPort() const
{
    return destinationPort_var;
}

void MyUDPPacket::setDestinationPort(int destinationPort)
{
    this->destinationPort_var = destinationPort;
}

int MyUDPPacket::getHopCount() const
{
    return hopCount_var;
}

void MyUDPPacket::setHopCount(int hopCount)
{
    this->hopCount_var = hopCount;
}

double MyUDPPacket::getSendTime() const
{
    return sendTime_var;
}

void MyUDPPacket::setSendTime(double sendTime)
{
    this->sendTime_var = sendTime;
}

double MyUDPPacket::getLastEnqueueTime() const
{
    return lastEnqueueTime_var;
}

void MyUDPPacket::setLastEnqueueTime(double lastEnqueueTime)
{
    this->lastEnqueueTime_var = lastEnqueueTime;
}

double MyUDPPacket::getPktRequirement() const
{
    return pktRequirement_var;
}

void MyUDPPacket::setPktRequirement(double pktRequirement)
{
    this->pktRequirement_var = pktRequirement;
}

int MyUDPPacket::getFlowId() const
{
    return flowId_var;
}

void MyUDPPacket::setFlowId(int flowId)
{
    this->flowId_var = flowId;
}

long MyUDPPacket::getSeqNo() const
{
    return seqNo_var;
}

void MyUDPPacket::setSeqNo(long seqNo)
{
    this->seqNo_var = seqNo;
}

double MyUDPPacket::getUpStreamDelay() const
{
    return upStreamDelay_var;
}

void MyUDPPacket::setUpStreamDelay(double upStreamDelay)
{
    this->upStreamDelay_var = upStreamDelay;
}

double MyUDPPacket::getDownStreamDelay() const
{
    return downStreamDelay_var;
}

void MyUDPPacket::setDownStreamDelay(double downStreamDelay)
{
    this->downStreamDelay_var = downStreamDelay;
}

bool MyUDPPacket::getIsSource() const
{
    return isSource_var;
}

void MyUDPPacket::setIsSource(bool isSource)
{
    this->isSource_var = isSource;
}

bool MyUDPPacket::getIsCenter() const
{
    return isCenter_var;
}

void MyUDPPacket::setIsCenter(bool isCenter)
{
    this->isCenter_var = isCenter;
}

double MyUDPPacket::getRealTimeDownStreamDelay() const
{
    return realTimeDownStreamDelay_var;
}

void MyUDPPacket::setRealTimeDownStreamDelay(double realTimeDownStreamDelay)
{
    this->realTimeDownStreamDelay_var = realTimeDownStreamDelay;
}

delayRptVector& MyUDPPacket::getRptVector()
{
    return rptVector_var;
}

void MyUDPPacket::setRptVector(const delayRptVector& rptVector)
{
    this->rptVector_var = rptVector;
}

dsRptVector& MyUDPPacket::getDownStreamSeen()
{
    return downStreamSeen_var;
}

void MyUDPPacket::setDownStreamSeen(const dsRptVector& downStreamSeen)
{
    this->downStreamSeen_var = downStreamSeen;
}

int MyUDPPacket::getTransferNode() const
{
    return transferNode_var;
}

void MyUDPPacket::setTransferNode(int transferNode)
{
    this->transferNode_var = transferNode;
}

int MyUDPPacket::getExpectedNeighbor() const
{
    return expectedNeighbor_var;
}

void MyUDPPacket::setExpectedNeighbor(int expectedNeighbor)
{
    this->expectedNeighbor_var = expectedNeighbor;
}

DelayMap& MyUDPPacket::getNeighborDelayInfo()
{
    return neighborDelayInfo_var;
}

void MyUDPPacket::setNeighborDelayInfo(const DelayMap& neighborDelayInfo)
{
    this->neighborDelayInfo_var = neighborDelayInfo;
}

DelayMapDV& MyUDPPacket::getNeighborDelayTable()
{
    return neighborDelayTable_var;
}

void MyUDPPacket::setNeighborDelayTable(const DelayMapDV& neighborDelayTable)
{
    this->neighborDelayTable_var = neighborDelayTable;
}

piggybackInfo& MyUDPPacket::getPbUpdate()
{
    return pbUpdate_var;
}

void MyUDPPacket::setPbUpdate(const piggybackInfo& pbUpdate)
{
    this->pbUpdate_var = pbUpdate;
}

class MyUDPPacketDescriptor : public cClassDescriptor
{
  public:
    MyUDPPacketDescriptor();
    virtual ~MyUDPPacketDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(MyUDPPacketDescriptor);

MyUDPPacketDescriptor::MyUDPPacketDescriptor() : cClassDescriptor("MyUDPPacket", "cPacket")
{
}

MyUDPPacketDescriptor::~MyUDPPacketDescriptor()
{
}

bool MyUDPPacketDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<MyUDPPacket *>(obj)!=NULL;
}

const char *MyUDPPacketDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int MyUDPPacketDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 22+basedesc->getFieldCount(object) : 22;
}

unsigned int MyUDPPacketDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISCOMPOUND,
        FD_ISCOMPOUND,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISCOMPOUND,
        FD_ISCOMPOUND,
        FD_ISCOMPOUND,
    };
    return (field>=0 && field<22) ? fieldTypeFlags[field] : 0;
}

const char *MyUDPPacketDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "sourceAddr",
        "destinationAddr",
        "sourcePort",
        "destinationPort",
        "hopCount",
        "sendTime",
        "lastEnqueueTime",
        "pktRequirement",
        "flowId",
        "seqNo",
        "upStreamDelay",
        "downStreamDelay",
        "isSource",
        "isCenter",
        "realTimeDownStreamDelay",
        "rptVector",
        "downStreamSeen",
        "transferNode",
        "expectedNeighbor",
        "neighborDelayInfo",
        "neighborDelayTable",
        "pbUpdate",
    };
    return (field>=0 && field<22) ? fieldNames[field] : NULL;
}

int MyUDPPacketDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='s' && strcmp(fieldName, "sourceAddr")==0) return base+0;
    if (fieldName[0]=='d' && strcmp(fieldName, "destinationAddr")==0) return base+1;
    if (fieldName[0]=='s' && strcmp(fieldName, "sourcePort")==0) return base+2;
    if (fieldName[0]=='d' && strcmp(fieldName, "destinationPort")==0) return base+3;
    if (fieldName[0]=='h' && strcmp(fieldName, "hopCount")==0) return base+4;
    if (fieldName[0]=='s' && strcmp(fieldName, "sendTime")==0) return base+5;
    if (fieldName[0]=='l' && strcmp(fieldName, "lastEnqueueTime")==0) return base+6;
    if (fieldName[0]=='p' && strcmp(fieldName, "pktRequirement")==0) return base+7;
    if (fieldName[0]=='f' && strcmp(fieldName, "flowId")==0) return base+8;
    if (fieldName[0]=='s' && strcmp(fieldName, "seqNo")==0) return base+9;
    if (fieldName[0]=='u' && strcmp(fieldName, "upStreamDelay")==0) return base+10;
    if (fieldName[0]=='d' && strcmp(fieldName, "downStreamDelay")==0) return base+11;
    if (fieldName[0]=='i' && strcmp(fieldName, "isSource")==0) return base+12;
    if (fieldName[0]=='i' && strcmp(fieldName, "isCenter")==0) return base+13;
    if (fieldName[0]=='r' && strcmp(fieldName, "realTimeDownStreamDelay")==0) return base+14;
    if (fieldName[0]=='r' && strcmp(fieldName, "rptVector")==0) return base+15;
    if (fieldName[0]=='d' && strcmp(fieldName, "downStreamSeen")==0) return base+16;
    if (fieldName[0]=='t' && strcmp(fieldName, "transferNode")==0) return base+17;
    if (fieldName[0]=='e' && strcmp(fieldName, "expectedNeighbor")==0) return base+18;
    if (fieldName[0]=='n' && strcmp(fieldName, "neighborDelayInfo")==0) return base+19;
    if (fieldName[0]=='n' && strcmp(fieldName, "neighborDelayTable")==0) return base+20;
    if (fieldName[0]=='p' && strcmp(fieldName, "pbUpdate")==0) return base+21;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *MyUDPPacketDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "int",
        "int",
        "int",
        "int",
        "double",
        "double",
        "double",
        "int",
        "long",
        "double",
        "double",
        "bool",
        "bool",
        "double",
        "delayRptVector",
        "dsRptVector",
        "int",
        "int",
        "DelayMap",
        "DelayMapDV",
        "piggybackInfo",
    };
    return (field>=0 && field<22) ? fieldTypeStrings[field] : NULL;
}

const char *MyUDPPacketDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int MyUDPPacketDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    MyUDPPacket *pp = (MyUDPPacket *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string MyUDPPacketDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    MyUDPPacket *pp = (MyUDPPacket *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getSourceAddr());
        case 1: return long2string(pp->getDestinationAddr());
        case 2: return long2string(pp->getSourcePort());
        case 3: return long2string(pp->getDestinationPort());
        case 4: return long2string(pp->getHopCount());
        case 5: return double2string(pp->getSendTime());
        case 6: return double2string(pp->getLastEnqueueTime());
        case 7: return double2string(pp->getPktRequirement());
        case 8: return long2string(pp->getFlowId());
        case 9: return long2string(pp->getSeqNo());
        case 10: return double2string(pp->getUpStreamDelay());
        case 11: return double2string(pp->getDownStreamDelay());
        case 12: return bool2string(pp->getIsSource());
        case 13: return bool2string(pp->getIsCenter());
        case 14: return double2string(pp->getRealTimeDownStreamDelay());
        case 15: {std::stringstream out; out << pp->getRptVector(); return out.str();}
        case 16: {std::stringstream out; out << pp->getDownStreamSeen(); return out.str();}
        case 17: return long2string(pp->getTransferNode());
        case 18: return long2string(pp->getExpectedNeighbor());
        case 19: {std::stringstream out; out << pp->getNeighborDelayInfo(); return out.str();}
        case 20: {std::stringstream out; out << pp->getNeighborDelayTable(); return out.str();}
        case 21: {std::stringstream out; out << pp->getPbUpdate(); return out.str();}
        default: return "";
    }
}

bool MyUDPPacketDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    MyUDPPacket *pp = (MyUDPPacket *)object; (void)pp;
    switch (field) {
        case 0: pp->setSourceAddr(string2long(value)); return true;
        case 1: pp->setDestinationAddr(string2long(value)); return true;
        case 2: pp->setSourcePort(string2long(value)); return true;
        case 3: pp->setDestinationPort(string2long(value)); return true;
        case 4: pp->setHopCount(string2long(value)); return true;
        case 5: pp->setSendTime(string2double(value)); return true;
        case 6: pp->setLastEnqueueTime(string2double(value)); return true;
        case 7: pp->setPktRequirement(string2double(value)); return true;
        case 8: pp->setFlowId(string2long(value)); return true;
        case 9: pp->setSeqNo(string2long(value)); return true;
        case 10: pp->setUpStreamDelay(string2double(value)); return true;
        case 11: pp->setDownStreamDelay(string2double(value)); return true;
        case 12: pp->setIsSource(string2bool(value)); return true;
        case 13: pp->setIsCenter(string2bool(value)); return true;
        case 14: pp->setRealTimeDownStreamDelay(string2double(value)); return true;
        case 17: pp->setTransferNode(string2long(value)); return true;
        case 18: pp->setExpectedNeighbor(string2long(value)); return true;
        default: return false;
    }
}

const char *MyUDPPacketDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        "delayRptVector",
        "dsRptVector",
        NULL,
        NULL,
        "DelayMap",
        "DelayMapDV",
        "piggybackInfo",
    };
    return (field>=0 && field<22) ? fieldStructNames[field] : NULL;
}

void *MyUDPPacketDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    MyUDPPacket *pp = (MyUDPPacket *)object; (void)pp;
    switch (field) {
        case 15: return (void *)(&pp->getRptVector()); break;
        case 16: return (void *)(&pp->getDownStreamSeen()); break;
        case 19: return (void *)(&pp->getNeighborDelayInfo()); break;
        case 20: return (void *)(&pp->getNeighborDelayTable()); break;
        case 21: return (void *)(&pp->getPbUpdate()); break;
        default: return NULL;
    }
}


