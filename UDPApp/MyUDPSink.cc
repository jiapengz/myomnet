//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <MyUDPSink.h>

Define_Module(MyUDPSink);

MyUDPSink::MyUDPSink() {
    // TODO Auto-generated constructor stub

}

MyUDPSink::~MyUDPSink() {
    // TODO Auto-generated destructor stub
}

void MyUDPSink::initialize(int stage)
{
    MyAppBase::initialize(stage);
    if (stage == 0)
    {
        //numReceived = 0;
        ;

    }
    sinkRecvNo = registerSignal("sinkRecvNo");

    fileName = par("recFileName").stringValue();
    myAddr = getParentModule()->par("address");
    destinationAddr = par("PDCtoCCAddr");
    recSrcID = par("recSrcID");
    isPDC = par("isPDC");
    upperbd = par("upperbd");
    lowerbd = par("lowerbd");
    numNeedtoRecv = par("numNeedtoRecv");
    PMUReq = par("PMUReq");
    pdcFlowId = par("flowId");

    if(isPDC)
    {
        EV << "Node " << myAddr << " will be used as a PDC. Upper: " << upperbd << ", lower: " << lowerbd << \
            ", need to receive: " << numNeedtoRecv << ". The CC address is: " << destinationAddr << ", req_time: " << PMUReq << endl;
    }

    if(fileName.compare("") != 0)
    {
        EV << "The file name is: " << fileName.c_str() << endl;
        if(oFile.is_open())
            oFile.close();

        oFile.open(fileName.c_str());
    }
}

void MyUDPSink::handleMessageWhenUp(cMessage *msg)
{
    //numReceived++;
    MyUDPPacket *recvMsg = check_and_cast<MyUDPPacket *>(msg);
    int recvSrc = recvMsg->getSourceAddr();
    //double vioRatio;

    if(!isPDC)
    {
        if(recvMsg->getKind() == TRAFFIC)
        {
            singlePktProcess(msg);
        }
        else if(recvMsg->getKind() == PDC_TO_CC)
        {
            EV << "The packet is from a PDC node, processing......" << endl;

            MyPDCPacket *recvPDCMsg = check_and_cast<MyPDCPacket *>(recvMsg);
            std::vector<MyUDPPacket*>::iterator vtrItr;

            for(vtrItr = recvPDCMsg->getPktPtrVtr().begin(); vtrItr != recvPDCMsg->getPktPtrVtr().end(); vtrItr++ )
            {
                singlePktProcess(*vtrItr, msg);

                // Caution: if we delete the pointer here, be careful if we want to delete a packet at other place
                delete *vtrItr;
            }
        }

        delete msg;
    /*************************End of recording***********************************/
    }
    else // This is a PDC node, we should gather all packets with the same time tag
    {
        singlePktProcess(msg);
        EV << "**********************Collecting data in the PDC**********************" << endl;
        if(pdcBuffer.find(recvMsg->getSeqNo()) == pdcBuffer.end())
        {
            pdcStorage tempPS;
            pdcBuffer[recvMsg->getSeqNo()] = tempPS;
            pdcBuffer[recvMsg->getSeqNo()].total = 1;
            pdcBuffer[recvMsg->getSeqNo()].earliestSendTime = recvMsg->getSendTime();
            pdcBuffer[recvMsg->getSeqNo()].pktBuffer[recvSrc] = recvMsg;

            EV << "***Add a new Sequence record to the buffer." << endl;
        }
        else
        {
            pdcBuffer[recvMsg->getSeqNo()].total += 1;
            if(pdcBuffer[recvMsg->getSeqNo()].earliestSendTime > recvMsg->getSendTime())
            {
                pdcBuffer[recvMsg->getSeqNo()].earliestSendTime = recvMsg->getSendTime();
            }
            pdcBuffer[recvMsg->getSeqNo()].pktBuffer[recvSrc] = recvMsg;

            EV << "Continue adding data" << endl;
        }

        if(pdcBuffer[recvMsg->getSeqNo()].total == numNeedtoRecv)
        {
            EV << "***All PMU data have been received and we can send to the control center now." << endl;
            sendPDCData(recvMsg->getSeqNo());
            EV << "************************************************************************" << endl;

            pdcBuffer.erase(recvMsg->getSeqNo());
        }
    }
}

void MyUDPSink::showDelayRpt(cMessage *msg)
{
    MyUDPPacket *pk = check_and_cast<MyUDPPacket *>(msg);

    if(pk)
    {
        std::vector<testDelayRpt>::iterator tdrIt;
        for(tdrIt = pk->getRptVector().begin(); tdrIt != pk->getRptVector().end(); tdrIt++)
        {
            EV << "!!!At node " << tdrIt->nid << ", estimated delay: " << tdrIt->Est << ", real delay: " <<\
                    simTime().dbl() - tdrIt->recTime << endl;
        }

        /*std::vector<testDelayRpt>::reverse_iterator tdrIt_rv;
        EV << "Reverse order:" << endl;
        for(tdrIt_rv = pk->getRptVector().rbegin(); tdrIt_rv != pk->getRptVector().rend(); tdrIt_rv++)
        {
            EV << "At node " << tdrIt_rv->nid << ", estimated delay: " << tdrIt_rv->Est << ", real delay: " <<\
                    simTime().dbl() - tdrIt_rv->recTime << endl;
        }*/
    }
}

void MyUDPSink::recDelayRpt(cMessage *msg)
{
    MyUDPPacket *pk = check_and_cast<MyUDPPacket *>(msg);

    oFile << pk->getSourceAddr() << " " << myAddr << " " << pk->getFlowId() << " " << pk->getSeqNo() << " "\
            << simTime().dbl() - pk->getSendTime() << " ";

    std::vector<testDelayRpt>::iterator tdrIt;

    for(tdrIt = pk->getRptVector().begin(); tdrIt != pk->getRptVector().end(); tdrIt++)
    {
        oFile << tdrIt->nid << " " << tdrIt->Est << " " << simTime().dbl() - tdrIt->recTime << " ";
    }

    oFile << endl;

}

void MyUDPSink::computeRSS(cMessage *msg)
{
    MyUDPPacket *pk = check_and_cast<MyUDPPacket *>(msg);

    if(pk)
    {
        unsigned int vecSize = pk->getRptVector().size();
        unsigned i;
        int pktSrc = pk->getSourceAddr();
        int pktFlowId = pk->getFlowId();
        double tmpVar;
        double tmpPerHop;

        for(i = 0; i < vecSize; i++)
        {
            tmpVar = pow((simTime().dbl() - pk->getRptVector()[i].recTime - pk->getRptVector()[i].Est), 2);
//            realEstSig[pktSrc].rssValue[i] += tmpVar;
            packetRec[pktSrc][pktFlowId].realEstSig.rssValue[i] += tmpVar;
            emit(packetRec[pktSrc][pktFlowId].realEstSig.estSig[i], pk->getRptVector()[i].Est * 1000); // emit estimation value
//            emit(realEstSig[pktSrc].estSig[i], pk->getRptVector()[i].Est * 1000);
            emit(packetRec[pktSrc][pktFlowId].realEstSig.realSig[i], (simTime().dbl() - pk->getRptVector()[i].recTime) * 1000);
            // emit real value
//            emit(realEstSig[pktSrc].realSig[i], (simTime().dbl() - pk->getRptVector()[i].recTime) * 1000);

            if(i < vecSize - 1)
            {
                tmpPerHop = pk->getRptVector()[i+1].recTime - pk->getRptVector()[i].recTime;
            }
            else if(i == vecSize - 1)
            {
                tmpPerHop = simTime().dbl() - pk->getRptVector()[i].recTime;
            }
            emit(packetRec[pktSrc][pktFlowId].realEstSig.perHopDelaySig[i], tmpPerHop * 1000); // emit the per-hop delay
            emit(packetRec[pktSrc][pktFlowId].realEstSig.dsDelaySeenSig[i], pk->getDownStreamSeen()[i] * 1000);
//            emit(realEstSig[pktSrc].perHopDelaySig[i], tmpPerHop * 1000); // emit the per-hop delay
//            emit(realEstSig[pktSrc].dsDelaySeenSig[i], pk->getDownStreamSeen()[i] * 1000);
        }
    }
}

void MyUDPSink::emitRealEstSignal()
{
//    std::map<int,realEstSigStruct>::iterator tmpIt;
    unsigned int vecSize, i;
    double emitResult;

//    for(tmpIt = realEstSig.begin(); tmpIt != realEstSig.end(); tmpIt++)
//    {
//        vecSize = tmpIt->second.outputSig.size();
//
//        for(i = 0; i < vecSize; i++)
//        {
//            emitResult = pow(tmpIt->second.rssValue[i] / recvRec[tmpIt->first].totalReceived, 0.5) * 1000;
//            emit(tmpIt->second.outputSig[i], emitResult);
//        }
//    }
    std::map<int,std::map<int,statisticRec> >::iterator outterItr;
    std::map<int,statisticRec>::iterator innerItr;

    for(outterItr = packetRec.begin(); outterItr != packetRec.end(); outterItr++)
    {
        for(innerItr = outterItr->second.begin(); innerItr != outterItr->second.end(); innerItr++)
        {
            vecSize = innerItr->second.realEstSig.rssSig.size();
            for(i = 0; i < vecSize; i++)
            {
                emitResult = pow(innerItr->second.realEstSig.rssValue[i] / (double)\
                    innerItr->second.recvRec.totalReceived, 0.5) * 1000;
                emit(innerItr->second.realEstSig.rssSig[i], emitResult);
            }
        }
    }
}

void MyUDPSink::emitVioSignal()
{
//    std::map<int,simsignal_t>::iterator tmpIt;
    double vioRatio;
//
//    for(tmpIt = violationSig.begin(); tmpIt != violationSig.end(); tmpIt++)
//    {
//        vioRatio = recvRec[tmpIt->first].violatedNum / (double)recvRec[tmpIt->first].totalReceived;
//        emit(tmpIt->second, vioRatio);
//    }
    std::map<int,std::map<int,statisticRec> >::iterator outterItr;
    std::map<int,statisticRec>::iterator innerItr;

    for(outterItr = packetRec.begin(); outterItr != packetRec.end(); outterItr++)
    {
        for(innerItr = outterItr->second.begin(); innerItr != outterItr->second.end(); innerItr++)
        {
            EV << "(" << outterItr->first << "," << innerItr->first << ")" << "Total received: " <<\
                    innerItr->second.recvRec.totalReceived <<\
                    "; violated: " << innerItr->second.recvRec.violatedNum << endl;
            vioRatio = innerItr->second.recvRec.violatedNum / (double)innerItr->second.recvRec.totalReceived;
            emit(innerItr->second.violationSig, vioRatio);
        }
    }
}

void MyUDPSink::emitMeanDelaySignal()
{
//    std::map<int,simsignal_t>::iterator tmpIt;
    double meanDelay;

//    for(tmpIt = meanDelaySig.begin(); tmpIt != meanDelaySig.end(); tmpIt++)
//    {
//        meanDelay = recvRec[tmpIt->first].totalDelay / recvRec[tmpIt->first].totalReceived * 1000;
//        emit(tmpIt->second, meanDelay);
//    }
    std::map<int,std::map<int,statisticRec> >::iterator outterItr;
    std::map<int,statisticRec>::iterator innerItr;

    for(outterItr = packetRec.begin(); outterItr != packetRec.end(); outterItr++)
    {
        for(innerItr = outterItr->second.begin(); innerItr != outterItr->second.end(); innerItr++)
        {
            meanDelay = innerItr->second.recvRec.totalDelay / (double)innerItr->second.recvRec.totalReceived * 1000;
            emit(innerItr->second.meanDelaySig, meanDelay);
        }
    }
}

void MyUDPSink::testScanPDCBuffer_v1()
{
    std::map<int,pdcStorage>::iterator pdcItr;
    std::map<int,MyUDPPacket* >::iterator tempItr;

    for(pdcItr = pdcBuffer.begin(); pdcItr != pdcBuffer.end();)
    {
        for(tempItr = pdcItr->second.pktBuffer.begin(); tempItr != pdcItr->second.pktBuffer.end();)
        {
            EV << "******Data from PMU_" << tempItr->second->getSourceAddr() << ", size: " << tempItr->second->getBitLength() <<\
                   ", destination: " << tempItr->second->getDestinationAddr() << ", SeqNo: " << tempItr->second->getSeqNo()\
                   << ", requirement: " << tempItr->second->getPktRequirement() << "******" << endl;

            delete tempItr->second;
            pdcItr->second.pktBuffer.erase(tempItr++);
        }

        pdcBuffer.erase(pdcItr++);
    }

    EV << "Complete the scan of the PDC buffer. Now the map contains " << pdcBuffer.size() << " elements." << endl;
}

// To clean up the data stored in the <map>
void MyUDPSink::clearPDCBuffer()
{
    std::map<int,pdcStorage>::iterator pdcItr;
    std::map<int,MyUDPPacket* >::iterator tempItr;

    for(pdcItr = pdcBuffer.begin(); pdcItr != pdcBuffer.end();)
    {
        for(tempItr = pdcItr->second.pktBuffer.begin(); tempItr != pdcItr->second.pktBuffer.end();)
        {
            //delete tempItr->second;
            pdcItr->second.pktBuffer.erase(tempItr++);
        }

        pdcBuffer.erase(pdcItr++);
    }
}

void MyUDPSink::sendPDCData(int seq)
{
    EV << "***Sending a packet from PDC at " << simTime() <<endl;

    char msgName[32];
    sprintf(msgName, "PDC-%d-Flow-%d-AppData-%d", myAddr, pdcFlowId, seq);

    MyPDCPacket *payload = new MyPDCPacket(msgName);
    //payload->setByteLength(packetLength);
    payload->setDestinationPort(1);
    payload->setSourcePort(2);
    payload->setSourceAddr(myAddr);
    payload->setDestinationAddr(destinationAddr);
    payload->setKind(PDC_TO_CC);
    payload->setSeqNo(seq);
    payload->setFlowId(myAddr);
    payload->setSendTime(simTime().dbl());
    payload->setLastEnqueueTime(0);
    //payload->setPktRequirement(requirement);

    payload->setUpStreamDelay(0);
    payload->setDownStreamDelay(0);


    std::map<int,pdcStorage>::iterator pdcItr;
    std::map<int,MyUDPPacket* >::iterator tmpItr;
    int64 totalSize = 0;
    double newReq = 0;
    double leastTime = 0;

    pdcItr = pdcBuffer.find(seq);
    if(pdcItr != pdcBuffer.end())
    {
        leastTime = pdcItr->second.earliestSendTime;
        for(tmpItr = pdcItr->second.pktBuffer.begin(); tmpItr != pdcItr->second.pktBuffer.end(); tmpItr++)
        {
            totalSize += tmpItr->second->getBitLength();
            payload->getPktPtrVtr().push_back(tmpItr->second);
        }

        EV << "***Totally " << pdcItr->second.pktBuffer.size() << " collected, size is " << totalSize \
                << ". Originally generated at " << leastTime << endl;
    }

    newReq = PMUReq - (simTime().dbl() - leastTime);

    payload->setPktRequirement(newReq);
    payload->setBitLength(totalSize);
    EV << "***The packet is " << payload->getByteLength() << " Byte, " << payload->getBitLength() << " bit" << endl;

    send(payload, "appOut");
}

void MyUDPSink::singlePktProcess(cMessage *msg, cMessage *pdcMsg)
{
    MyUDPPacket *recvMsg = check_and_cast<MyUDPPacket *>(msg);
    int recvSrc = recvMsg->getSourceAddr();
    int recvFlow = recvMsg->getFlowId();

    if(pdcMsg == NULL)
    {
        EV << "We are processing a normal packet" << endl;
    }
    else
    {
        EV << "We are processing a packet from the PDC" << endl;
    }


    EV << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
    EV << "!!!Receive a message at " << simTime() << ", #" << recvMsg->getSeqNo() << endl;
    EV << "!!!SrcAddr: " << recvMsg->getSourceAddr() << ", DestAddr: " << recvMsg->getDestinationAddr() << endl;
    EV << "!!!SourcePort: " << recvMsg->getSourcePort() << " DestPort: " << recvMsg->getDestinationPort() << endl;
    EV << "!!!FlowId: " << recvMsg->getFlowId() << " sendtime: " << recvMsg->getSendTime() << " req: " << recvMsg->getPktRequirement()\
            << endl;
    EV << "!!!Deadline: " << recvMsg->getPktRequirement()+recvMsg->getSendTime() << ", now: "<< simTime().dbl() << endl;
    EV << "!!!SeqNo: " << recvMsg->getSeqNo() << ", delayed: " << \
            (recvMsg->getPktRequirement()+recvMsg->getSendTime() < simTime().dbl()) <<endl;
    //EV << "GetIndex " << getIndex() << " GetID " << getId() << endl;
    EV << endl;
    EV << "!!!Print out the estimation"<< endl;
    showDelayRpt(msg);

    EV << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;

    if(oFile.is_open())
    {
        recDelayRpt(msg);
    }


/*****************************************************************************************/
    EV << "Start statistical recording" << endl << endl;

    // If there is no record, we should add new record
    if(packetRec.find(recvSrc) == packetRec.end() || \
            packetRec.find(recvSrc)->second.find(recvFlow) == packetRec.find(recvSrc)->second.end())
    {
        EV << "Establish signal" << endl;
        establishSignal(msg);
    }

    packetRec.find(recvSrc)->second.find(recvFlow)->second.recvRec.totalReceived++;
    EV << "The received packet number is " << packetRec.find(recvSrc)->second.find(recvFlow)->second.recvRec.totalReceived << endl;
    packetRec.find(recvSrc)->second.find(recvFlow)->second.recvRec.totalDelay += (simTime().dbl() - recvMsg->getSendTime());
    EV << "The total Delay is " << \
             packetRec.find(recvSrc)->second.find(recvFlow)->second.recvRec.totalDelay << endl;
    if(recvMsg->getPktRequirement() < simTime().dbl() - recvMsg->getSendTime())
    {
        EV << "Packet (" << recvSrc << "," << recvFlow << "," << recvMsg->getSeqNo() << ") is delayed" << endl;
        packetRec.find(recvSrc)->second.find(recvFlow)->second.recvRec.violatedNum++;
    }
    EV << "There are " << packetRec.find(recvSrc)->second.find(recvFlow)->second.recvRec.violatedNum << " delayed" << endl;

/*Now we start the recording of statistics*/

    if((recSrcID == -2) || (recSrcID == recvSrc))
    {
        computeRSS(msg);
    }
}

void MyUDPSink::establishSignal(cMessage *msg)
{
    MyUDPPacket *recvMsg = check_and_cast<MyUDPPacket *>(msg);
    int recvSrc = recvMsg->getSourceAddr();
    int recvFlow = recvMsg->getFlowId();
    simsignal_t signal;
    cProperty *statisticTemplate;


    // Initialize the record of received packets
    statisticRec tempRec;
    tempRec.recvRec.totalDelay = 0;
    tempRec.recvRec.totalReceived = 0;
    tempRec.recvRec.violatedNum = 0;

    std::map<int,statisticRec> tempStaRec;
    tempStaRec[recvFlow] = tempRec;

    if(packetRec.find(recvSrc) == packetRec.end())
    {
        packetRec[recvSrc] = tempStaRec;
    }
    else
    {
        packetRec.find(recvSrc)->second.insert(std::make_pair(recvFlow,tempRec));
    }


// Establish violation signal
    sprintf(signalName, "src%d-flow%d-violation", recvSrc, recvFlow);
    sprintf(statisticName, "src%d-flow%d-violation", recvSrc, recvFlow);
    signal = registerSignal(signalName);
    packetRec[recvSrc][recvFlow].violationSig = signal;
    // "srcViolation" is used with the template in NED file
    statisticTemplate = getProperties()->get("statisticTemplate", "srcViolation");
    ev.addResultRecorders(this, signal, statisticName, statisticTemplate);


 // Establish mean-delay signal
    sprintf(signalName, "src%d-flow%d-mean-delay", recvSrc, recvFlow);
    sprintf(statisticName, "src%d-flow%d-mean-delay", recvSrc, recvFlow);
    signal = registerSignal(signalName);
    packetRec[recvSrc][recvFlow].meanDelaySig = signal;
    // "meanDelay" is used with the template in NED file
    statisticTemplate = getProperties()->get("statisticTemplate", "meanDelay");
    ev.addResultRecorders(this, signal, statisticName, statisticTemplate);



 // Start establishment of per-hop signals
    unsigned int vecSize = recvMsg->getRptVector().size();
    unsigned i;

//    if(realEstSig.find(pktSrc) == realEstSig.end())
//    {
//        realEstSigStruct sigVector;
//        realEstSig[pktSrc] = sigVector;
        for(i = 0; i < vecSize; i++) //register RSS signal
        {
            sprintf(signalName, "src%d-flow%d-rss-R%d", recvSrc, recvFlow, i);
            sprintf(statisticName, "src%d-flow%d-rss-R%d", recvSrc, recvFlow, i);
            signal = registerSignal(signalName);

//            realEstSig[pktSrc].outputSig.push_back(signal);
//            realEstSig[pktSrc].rssValue.push_back(0);
            packetRec[recvSrc][recvFlow].realEstSig.rssSig.push_back(signal);
            packetRec[recvSrc][recvFlow].realEstSig.rssValue.push_back(0);
            statisticTemplate = getProperties()->get("statisticTemplate", "realEstRSS");

            ev.addResultRecorders(this, signal, statisticName, statisticTemplate);
        }

        for(i = 0; i < vecSize; i++) //register estimation signal
        {
            sprintf(signalName, "src%d-flow%d-est-R%d", recvSrc, recvFlow, i);
            sprintf(statisticName, "src%d-flow%d-est-R%d", recvSrc, recvFlow, i);
            signal = registerSignal(signalName);

//            realEstSig[pktSrc].estSig.push_back(signal);
            packetRec[recvSrc][recvFlow].realEstSig.estSig.push_back(signal);
            cProperty *statisticTemplate = getProperties()->get("statisticTemplate", "estDelay");

            ev.addResultRecorders(this, signal, statisticName, statisticTemplate);
        }

        for(i = 0; i < vecSize; i++) // register real delay signal
        {
            sprintf(signalName, "src%d-flow%d-real-R%d", recvSrc, recvFlow, i);
            sprintf(statisticName, "src%d-flow%d-real-R%d", recvSrc, recvFlow, i);
            signal = registerSignal(signalName);

//            realEstSig[pktSrc].realSig.push_back(signal);
            packetRec[recvSrc][recvFlow].realEstSig.realSig.push_back(signal);
            statisticTemplate = getProperties()->get("statisticTemplate", "realDelay");

            ev.addResultRecorders(this, signal, statisticName, statisticTemplate);
        }

        for(i = 0; i < vecSize; i++) //register per-hop delay signal
        {
            sprintf(signalName, "src%d-flow%d-phd-R%d", recvSrc, recvFlow, i);
            sprintf(statisticName, "src%d-flow%d-phd-R%d", recvSrc, recvFlow, i);
            signal = registerSignal(signalName);

//            realEstSig[pktSrc].perHopDelaySig.push_back(signal);
            packetRec[recvSrc][recvFlow].realEstSig.perHopDelaySig.push_back(signal);
            statisticTemplate = getProperties()->get("statisticTemplate", "perHopDelay");

            ev.addResultRecorders(this, signal, statisticName, statisticTemplate);
        }

        for(i = 0; i < vecSize; i++) //register downstream delay (seen by a packet) signal
        {
            sprintf(signalName, "src%d-flow%d-dsds-R%d", recvSrc, recvFlow, i);
            sprintf(statisticName, "src%d-flow%d-dsds-R%d", recvSrc, recvFlow, i);
            signal = registerSignal(signalName);

//            realEstSig[pktSrc].dsDelaySeenSig.push_back(signal);
            packetRec[recvSrc][recvFlow].realEstSig.dsDelaySeenSig.push_back(signal);
            statisticTemplate = getProperties()->get("statisticTemplate", "dsDelaySeen");

            ev.addResultRecorders(this, signal, statisticName, statisticTemplate);
        }
//    }

}

void MyUDPSink::processPDCData(cMessage *msg)
{
    ;
}

void MyUDPSink::finish()
{
    MyAppBase::finish();
    emitVioSignal();
    emitRealEstSignal();
    emitMeanDelaySignal();

    clearPDCBuffer();
}

bool MyUDPSink::startApp()
{
    EV << "Sink starts" << endl;
    return true;
}

bool MyUDPSink::stopApp()
{
    return true;
}

bool MyUDPSink::crashApp()
{
    return true;
}
