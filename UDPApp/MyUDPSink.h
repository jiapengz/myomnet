//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef MYUDPSINK_H_
#define MYUDPSINK_H_

#include "MyAppBase.h"
#include <fstream>
#include "MyPDCPacket_m.h"

struct realEstSigStruct
{
    std::vector<simsignal_t> rssSig;
    std::vector<simsignal_t> realSig;
    std::vector<simsignal_t> estSig;
    std::vector<simsignal_t> perHopDelaySig;
    std::vector<simsignal_t> dsDelaySeenSig;
    std::vector<double> rssValue;
};

struct statisticRec
{
    realEstSigStruct realEstSig;
    receivingRecord recvRec;
    simsignal_t violationSig; // for each received source there will be a signal
    simsignal_t meanDelaySig; // statistics for mean delay of a flow
};

struct pdcStorage
{
    int total;
    double earliestSendTime;
    std::map<int,MyUDPPacket*> pktBuffer;
};

class MyUDPSink : public MyAppBase
{
    protected:
        //unsigned int numReceived;
//        std::map<int,receivingRecord> recvRec;
//        std::map<int,simsignal_t> violationSig; // for each received source there will be a signal
//        std::map<int,simsignal_t> meanDelaySig; // statistics for mean delay of a flow
//        std::map<int,realEstSigStruct> realEstSig;
        std::map<int,std::map<int,statisticRec> > packetRec;
        std::ofstream oFile;
        std::string fileName;

        // Used when the node is a PDC
        std::map<int,pdcStorage> pdcBuffer;

        int recSrcID; // "-1" means do nothing, "-2" means record all flows, positive integer means the corresponding flow
        int upperbd;
        int lowerbd;
        int numNeedtoRecv;
        int pdcFlowId; // The flow id of the PDC traffic
        bool isPDC;
        double PMUReq;

        char signalName[32];
        char statisticName[32];

    public:
        MyUDPSink();
        virtual ~MyUDPSink();

    protected:
        virtual void initialize(int stage);
        virtual int numInitStages() const { return 3; }     //TODO STAGE_APPLAYER
        virtual void finish();
        virtual void handleMessageWhenUp(cMessage *msg);
        virtual void showDelayRpt(cMessage *msg);
        virtual void recDelayRpt(cMessage *msg);
        virtual void computeRSS(cMessage *msg);
        virtual void singlePktProcess(cMessage *msg, cMessage *pdcMsg = NULL);
        virtual void establishSignal(cMessage *msg);

        virtual void emitRealEstSignal();
        virtual void emitVioSignal();
        virtual void emitMeanDelaySignal();

        // A test function for the PDC Buffer
        virtual void testScanPDCBuffer_v1();
        virtual void clearPDCBuffer();
        virtual void sendPDCData(int seq);
        virtual void processPDCData(cMessage *msg);

        virtual bool startApp();
        virtual bool stopApp();
        virtual bool crashApp();

    private:
        simsignal_t sinkRecvNo;
};

#endif /* MYUDPSINK_H_ */
