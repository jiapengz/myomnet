//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef CBRTRAFFIC_H_
#define CBRTRAFFIC_H_

#include "MyAppBase.h"

class CBRTraffic : public MyAppBase
{
    protected:
        enum SelfMsgKinds { START = 1, SEND, STOP };

        int packetLength;
        double sendrate;
        double firstHopRate; // the rate out of the first hop
        simtime_t startTime;
        simtime_t stopTime;
        cMessage *selfMsg;
        unsigned int numSent;
        int flowId;
        double requirement;
        double manualDU;
        double manualDD;

    public:
        CBRTraffic();
        virtual ~CBRTraffic();

    protected:
        virtual int numInitStages() const {return 3;}
        virtual void initialize(int stage);
        virtual void handleMessageWhenUp(cMessage *msg);
        virtual void finish();
        virtual void sendPacket();

        virtual void processStart();
        virtual void processSend();
        virtual void processStop();

        //AppBase:
        bool startApp();
        bool stopApp();
        bool crashApp();
};

#endif /* CBRTRAFFIC_H_ */
