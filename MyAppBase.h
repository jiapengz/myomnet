//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef APPBASE_H_
#define APPBASE_H_

#include "MyUDPPacket_m.h"
#include "SimType.h"

class MyAppBase : public cSimpleModule
{
    protected:
        bool isOperational;
        int myAddr; // source address
        int destinationAddr; // the destination address
        bool randomStart;
        int rdStartIndex;
        simtime_t extraDelayRange;
        simtime_t extraStartDelay;
        int binNo;

    public:
        MyAppBase();
        virtual ~MyAppBase();

    protected:
        virtual void initialize(int stage);
        virtual int numInitStages() const { return 3; }     //TODO STAGE_APPLAYER
        virtual void finish();
        virtual void handleMessage(cMessage *msg);
        virtual void handleMessageWhenUp(cMessage *msg) = 0;
        virtual void handleMessageWhenDown(cMessage *msg);

        virtual bool startApp() = 0;
        virtual bool stopApp() = 0;
        virtual bool crashApp() = 0;
};

//Define_Module(MyAppBase);

#endif /* APPBASE_H_ */
