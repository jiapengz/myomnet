//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SIGLISTENER_H_
#define SIGLISTENER_H_

#include <omnetpp.h>
#include "MyUDPPacket_m.h"
#include <fstream>

class sigListener : public cListener
{
    public:
        sigListener();
        sigListener(char *filePtr);
        virtual ~sigListener();

    public:
        virtual void receiveSignal(cComponent *src, simsignal_t id, cObject *obj);

    private:
        std::ofstream outFile;
};

class sigResultFilter : public cObjectResultFilter
{
    public:
        using cObjectResultFilter::receiveSignal;
        virtual void receiveSignal(cResultFilter *prev, simtime_t_cref t, cObject *object);
};

#endif /* SIGLISTENER_H_ */
