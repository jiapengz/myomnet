/*
 * SimType.h
 *
 *  Created on: May 13, 2014
 *      Author: Zhang Jiapeng
 */

#ifndef SIMTYPE_H_
#define SIMTYPE_H_

#define NBD_ADDR 60000
#define LINK_STATE_UPDATE_ADDR 60001
#define DISTANCE_VECTOR_UPDATE_ADDR 60002
#define INFINITY_DISTANCE 65535
enum OutMsgKinds { TRAFFIC = 1, TEST, INIT_HELLO, LINK_DELAY_UPDATE, PIGGY_BACK, PDC_TO_CC };
enum SMSCmpResult {SAME_FLOW = 1, NEW_URGENT = -1, NEW_IS_UPDATE = -2, OLD_IS_UPDATE = 4, OLD_RUGENT = 3, NO_SLACK = 2};
enum myNodeKind {SOURCE = 0, ROUTER, CENTER};

struct pathRouteInfo
{
    int nextNode;
    int destPreNode;
    double delay;
};

struct receivingRecord
{
    unsigned int totalReceived;
    unsigned int violatedNum;
    double totalDelay;
};

#endif /* SIMTYPE_H_ */
